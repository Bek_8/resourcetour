<div class="nav-primary">
    <div class="container">
        <div class="row d-lg-flex d-none justify-content-between flex-wrap">
            <div class="col-lg-3 d-flex align-items-center justify-content-start">
                <a href="{{action('PageController@show',1)}}" class="nav-light__links active">
                    <div class="d-flex align-items-center">
                        <i data-feather="home"></i>
                        <span>My reservation</span>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-lg-center justify-content-start">
                <a href="{{action('PageController@reservation')}}" class="nav-light__links">
                    <div class="d-flex align-items-center">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M18.3332 10H1.6665" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M5.37484 4.25833L1.6665 9.99999V15C1.6665 15.442 1.8421 15.8659 2.15466 16.1785C2.46722 16.4911 2.89114 16.6667 3.33317 16.6667H16.6665C17.1085 16.6667 17.5325 16.4911 17.845 16.1785C18.1576 15.8659 18.3332 15.442 18.3332 15V9.99999L14.6248 4.25833C14.4869 3.98065 14.2742 3.74697 14.0106 3.58356C13.7471 3.42015 13.4432 3.33349 13.1332 3.33333H6.8665C6.55643 3.33349 6.25256 3.42015 5.98904 3.58356C5.72553 3.74697 5.51282 3.98065 5.37484 4.25833Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M5 13.3333H5.00833" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M5.00831 18.75L5.0083 17.5" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M15.0083 18.75L15.0083 17.5" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M15 13.3333H15.0083" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
                        <span>My transfers</span>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-lg-center justify-content-start">
                <a href="{{action('PageController@confirmation',1)}}" class="nav-light__links">
                    <div class="d-flex align-items-center">
                        <i data-feather="settings"></i>
                        <span>Settings</span>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-lg-end justify-content-start">
                <a href="{{action('PageController@confirmation',1)}}" class="nav-light__links">
                    <div class="d-flex align-items-center">
                        <i data-feather="log-out"></i>
                        <span>Logout</span>
                    </div>
                </a>
            </div>
        </div>
        <div class="row d-lg-none d-block justify-content-between flex-wrap mob-view">
            <div class="nav-light__links active">
                <a href="{{action('PageController@show',1)}}">
                    <div class="d-flex align-items-center">
                        <i data-feather="home"></i>
                        <span>My reservation</span>
                    </div>
                </a>
            </div>
            <div class="nav-light__links">
                <a href="{{action('PageController@reservation')}}">
                    <div class="d-flex align-items-center">
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M18.3332 10H1.6665" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M5.37484 4.25833L1.6665 9.99999V15C1.6665 15.442 1.8421 15.8659 2.15466 16.1785C2.46722 16.4911 2.89114 16.6667 3.33317 16.6667H16.6665C17.1085 16.6667 17.5325 16.4911 17.845 16.1785C18.1576 15.8659 18.3332 15.442 18.3332 15V9.99999L14.6248 4.25833C14.4869 3.98065 14.2742 3.74697 14.0106 3.58356C13.7471 3.42015 13.4432 3.33349 13.1332 3.33333H6.8665C6.55643 3.33349 6.25256 3.42015 5.98904 3.58356C5.72553 3.74697 5.51282 3.98065 5.37484 4.25833Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M5 13.3333H5.00833" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M5.00831 18.75L5.0083 17.5" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M15.0083 18.75L15.0083 17.5" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M15 13.3333H15.0083" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
                        <span>My transfers</span>
                    </div>
                </a>
            </div>
            <div class="nav-light__links">
                <a href="{{action('PageController@confirmation',1)}}">
                    <div class="d-flex align-items-center">
                        <i data-feather="settings"></i>
                        <span>Settings</span>
                    </div>
                </a>
            </div>
            <div class="nav-light__links">
                <a href="{{action('PageController@confirmation',1)}}">
                    <div class="d-flex align-items-center">
                        <i data-feather="log-out"></i>
                        <span>Logout</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
