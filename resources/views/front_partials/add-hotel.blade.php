<div class="add">
    <div class="container">
        <div class="row d-flex justify-content-between align-items-center">
            <div class="d-sm-none photo">
                <img src="{{ asset('images/add-hotel.png') }}" alt="image">
            </div>
            <div class="col-md-4 order-fix-md-1 blue-block">
                <div class="h2">Add your hotel to attract more customers</div>
            </div>
            <div class="col-md-5 order-fix-md-3 justify-content-center blue-block">
                <div class="p">Use the capabilities of our service for customer acquisition and online booking</div>
            </div>
            <div class="col-md-3 order-fix-md-2 d-flex justify-content-center blue-block">
                <a href="#" class="button">Add object</a>
            </div>
        </div>
    </div>
</div>
