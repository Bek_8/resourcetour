@if(session('success') || session('error'))
    <div class="message">
        <div class="container">
            <div class="row">
                <div class="message__info {{session('success') ? 'success' : 'error'}}">
                    {{session('success') ?? session('error')}}
                </div>
            </div>
        </div>
    </div>
@endif
