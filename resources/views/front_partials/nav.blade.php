<header>
    <nav class="nav">
        <div class="container">
            <div class="row d-flex justify-content-between flex-wrap">
                <div class="col-sm-7 d-flex align-items-center">
                    <a href="{{action('PageController@index')}}" class="nav__logo">
                        <img src="{{asset('backend/images/logo.svg')}}" alt="logo" />
                    </a>
                    <ul class="nav__links d-flex justify-content-between">
                        <li class="nav__links-item">
                            <a href="#" class="active"><i class="fa fa-hotel"></i> Hotels</a>
                        </li>
                        <li class="nav__links-item">
                            <a href="#"><i class="fa fa-car"></i> Transfers & Cars</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-5 d-flex justify-content-end align-items-center">
                <div class="nav__lang">
                    <div class="languages">
                        <a href="#" class="item">En</a>
                        <a href="#" class="item">Ru</a>
                    </div>
                </div>
                <div class="nav__add"><a href="#">Add hotel</a></div>
                <div class="nav__login"><a href="#">Sign in</a></div>
            </div>
            </div>
        </div>
    </nav>
    <nav class="nav-mobile d-lg-none">
        <div class="container h-100">
            <div class="row d-flex justify-content-between align-items-center h-100">
                <div class="logo">
                    <img src="{{asset('images/mobile-logo.svg')}}" alt="logo" />
                </div>
                <div class="nav-mobile-blocks d-flex align-items-center justify-content-between">
                <div class="block">
                   <a href="#" class="links active"><i class="fa fa-hotel"></i></a>
                </div>
                <div class="block">
                    <a href="#" class="links"><i class="fa fa-car"></i></a>
                </div>
                <div class="block">
                    <a href="#" class="links"><i class="fa fa-search"></i></a>
                </div>
                <div class="block">
                    <button class="hamburger links"><img src="{{asset('images/hamburger.svg')}}" alt="logo" /></button>
                </div>
            </div>
            </div>
        </div>
    </nav>
    <div class="search">
        <div class="container">
            <form action="{{action('PageController@search')}}" autocomplete="off" method="GET" class="row search-form d-flex align-items-center flex-wrap">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <keyword></keyword>
                    </div>
                </div>
                <div class="col-md-4 high-index">
                    <div class="search__box fixed-padding search__left d-flex align-items-center">
                        <label for="keyword" class="d-inline-flex justify-content-center align-items-center"><i class="search__icon" data-feather="search"></i></label>
                        <input type="text" placeholder="Сity or hotel" value="{{$keyword ??  ''}}" required="required" name="keyword" id="keyword" class="keyword" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="search__box fixed-padding search__center d-flex align-items-center">
                        <label for="calendar" class="d-inline-flex justify-content-center align-items-center"><i class="search__icon" data-feather="calendar"></i></label>
                        <div class="d-flex">
                            <input value="Check-in — Check out" id="daterange_nav" readonly="readonly" class="daterange_nav date-input" name="date" type="text" />
                            <span class="arrow d-flex align-items-center"><img src="{{asset('images/arrow-index.svg')}}" alt=""></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex align-items-center justify-content-between">
                    <div class="search__box fixed-padding search__right d-flex align-items-center">
                        <label for="adults" class="d-inline-flex justify-content-center align-items-center"><i class="search__icon" data-feather="user"></i></label>
                        <div class="d-flex">
                            <div class="adults adult-changer"><span class="adult-count">{{$information[0] ??  1}}</span> adults, <span class="children-count">{{$information[1] ??  0}}</span> child, <span class="room-count">{{$information[2] ??  1}}</span> rooms</div>
                            <input value="1,0,1" type="hidden" class="adult-changer" name="information" />
                            <span class="arrow d-flex align-items-center"><img src="{{asset('images/arrow-index.svg')}}" alt=""></span>
                        </div>
                    </div>
                    <div class="amount">
                        <div class="amount__box d-flex justify-content-between">
                            <div class="left d-flex align-items-center">
                                <div class="icon d-flex align-items-center justify-content-center"><img src="{{asset('images/adult.svg')}}" class="pointer" alt="adults" /></div>
                                <div class="title d-flex align-items-center">Adults</div>
                            </div>
                            <div class="right d-flex justify-content-between align-items-center">
                                <div class="plus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-plus.svg')}}" class="pointer" alt="plus" /></div>
                                <div class="counter adult-count d-flex justify-content-center align-items-center" data-count="adult-count">{{$information[0] ??  1}}</div>
                                <div class="minus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-minus.svg')}}" alt="minus" /></div>
                            </div>
                        </div>
                        <div class="amount__box d-flex justify-content-between">
                            <div class="left d-flex align-items-center">
                                <div class="icon d-flex align-items-center justify-content-center"><img src="{{asset('images/children.svg')}}" alt="children" /></div>
                                <div class="title d-flex align-items-center">Children</div>
                            </div>
                            <div class="right d-flex justify-content-between align-items-center">
                                <div class="plus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-plus.svg')}}" class="pointer" alt="plus" /></div>
                                <div class="counter children-count d-flex justify-content-center align-items-center" data-count="children-count">{{$information[1] ??  0}}</div>
                                <div class="minus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-minus.svg')}}" alt="minus" /></div>
                            </div>
                        </div>
                        <div class="amount__box d-flex justify-content-between">
                            <div class="left d-flex align-items-center">
                                <div class="icon d-flex align-items-center justify-content-center"><img src="{{asset('images/rooms.svg')}}" class="pointer" alt="plus" /></div>
                                <div class="title d-flex align-items-center">Rooms</div>
                            </div>
                            <div class="right d-flex justify-content-between align-items-center">
                                <div class="plus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-plus.svg')}}" class="pointer" alt="plus" /></div>
                                <div class="counter room-count d-flex justify-content-center align-items-center" data-count="room-count">{{$information[2] ??  1}}</div>
                                <div class="minus amount__box-button d-flex justify-content-center align-items-center"><img src="{{asset('images/index-minus.svg')}}" alt="minus" /></div>
                            </div>
                        </div>
                    </div>
                <button class="search__button d-inline-flex justify-content-center align-items-center">
                    <svg width="20" height="20" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8.25 14.25C11.5637 14.25 14.25 11.5637 14.25 8.25C14.25 4.93629 11.5637 2.25 8.25 2.25C4.93629 2.25 2.25 4.93629 2.25 8.25C2.25 11.5637 4.93629 14.25 8.25 14.25Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M15.7498 15.75L12.4873 12.4875" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>

                </button>
                </div>
            </form>
        </div>
    </div>
</header>
