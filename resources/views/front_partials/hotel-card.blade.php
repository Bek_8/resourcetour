<a href="{{action('PageController@show',$hotel->id)}}" class="item">
{{--    <div class="sale">-25%</div>--}}
    <div class="top">
        @if(false)
            @include('front_partials.recommend')
        @endif
    </div>
    <div class="bottom">
        <div class="left">
            <div class="title">{{ $hotel->name }}</div>
            <div class="location d-flex">
                <div class="icon d-flex align-items-center justify-content-center">
                    <svg width="18" height="18"
                         viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M16 7.36364C16 13.0909 8.5 18 8.5 18C8.5 18 1 13.0909 1 7.36364C1 5.41068 1.79018 3.53771 3.1967 2.15676C4.60322 0.775809 6.51088 0 8.5 0C10.4891 0 12.3968 0.775809 13.8033 2.15676C15.2098 3.53771 16 5.41068 16 7.36364Z" fill="#43F4EE"/>
                    <path d="M8.5 11C10.433 11 12 9.433 12 7.5C12 5.567 10.433 4 8.5 4C6.567 4 5 5.567 5 7.5C5 9.433 6.567 11 8.5 11Z" fill="#00847F"/>
                    </svg>
                </div>
                {{ $hotel->city->name_en}}, {{$hotel->city->country->name_en}}</div>
        </div>
        <div class="right">
            <div class="stars d-flex justify-content-end">
                <div class="icon">
                    <svg width="32" height="32"
                         viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="32" height="32" rx="16" fill="white"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M16 10C15.7774 10 15.5925 10.1564 15.5349 10.3704L14.3549 14.1739H10.4799C10.2124 14.1739 10 14.4167 10 14.6982C10 14.8705 10.0825 15.0243 10.2075 15.1158C10.2849 15.1705 13.3374 17.4974 13.3374 17.4974C13.3374 17.4974 12.1651 21.2541 12.1423 21.314C12.125 21.3687 12.1125 21.4288 12.1125 21.4913C12.1125 21.773 12.3298 22 12.5974 22C12.7 22 12.7949 21.9661 12.875 21.9088L16 19.5347C16 19.5347 19.055 21.8566 19.125 21.9088C19.2048 21.9661 19.3001 22 19.4023 22C19.6699 22 19.8875 21.7705 19.8875 21.4913C19.8875 21.4288 19.8751 21.3687 19.8574 21.314C19.835 21.2541 18.6626 17.4974 18.6626 17.4974C18.6626 17.4974 21.7149 15.1705 21.7925 15.1158C21.9175 15.0243 22 14.8705 22 14.6957C22 14.4167 21.7925 14.1739 21.5249 14.1739H17.6499L16.4648 10.3704C16.4075 10.1564 16.2224 10 16 10Z" fill="currentColor"/>
                    </svg>
                    <div class="rating">9/10</div>
                </div>
            </div>
            <div class="price">
                Start from <span class="golden">USD 175</span>
            </div>
        </div>
    </div>
</a>
