<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Tourist Resource Managment</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    @yield('style')
<!-- Web Application Manifest -->
    <!-- Add to homescreen for Chrome on Android -->
    <meta name="application-name" content="Tourist Resource Managment">
    <link rel="icon" sizes="512x512" href="/img/icons/icon-512x512.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-title" content="Tourist Resource Managment">
    <link rel="apple-touch-icon" href="/img/icons/icon-512x512.png">
    <link href="/img/icons/splash-640x1136.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-750x1334.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1242x2208.png" media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1125x2436.png" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-828x1792.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1242x2688.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1536x2048.png" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1668x2224.png" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-1668x2388.png" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
    <link href="/img/icons/splash-2048x2732.png" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />

    <!-- Tile for Win8 -->
    <meta name="msapplication-TileImage" content="/img/icons/icon-512x512.png">
    <script src="{{ asset('backend/js/require.min.js')}}"></script>
    <script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filer/jquery.filer.js') }}"></script>
    <link rel="stylesheet" href="{{asset('backend/css/jquery.filer.css')}}">
    <link rel="stylesheet" href="{{asset('backend/css/jquery.filer-dragdropbox-theme.css')}}">
    <script>
      requirejs.config({
          baseUrl: '/'
      });
    </script>
    <link href="{{ asset('backend/css/dashboard.css')}}" rel="stylesheet" />
    <link href="{{ asset('backend/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/js/dashboard.js')}}"></script>
    <link href="{{ asset('backend/plugins/charts-c3/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/charts-c3/plugin.js')}}"></script>
    <link href="{{ asset('backend/plugins/maps-google/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/maps-google/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/input-mask/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/datatables/plugin.js') }}"></script>
    <script src="{{ asset('backend/js/custom.js')}}"></script>
    <script src="{{ asset('backend/js/main.js')}}"></script>
    <link href="{{ asset('backend/css/custom.css')}}" rel="stylesheet" />
    <link href="{{ asset('backend/css/style.css')}}" rel="stylesheet" />
  </head>
  <body>
<div class="page">
    <div class="flex-fill">

    @if(Auth::user() !== null)
        @if(Auth::user()->hotels()->count() != 0)
            @if(Auth::user()->hotels()->first()->rooms()->count() != 0 || Auth::user()->hasRole('admin'))
            <header class="second">
    <div class="top-block">
        <div class="top-line">
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="left-block">
                    <a href="{{ action('HomeController@index')  }}">
                        <img src="{{ asset('backend/images/logo.svg') }}" />
                    </a>
                    <span class="description">Hotel Management System</span>
                </div>
                <div class="right-block">
                    <span class="btn badge-default tag-rounded" style="color: #00236D;font-weight: normal;">Помощь</span>
                    <div class="dropdown">
                        <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                        <span class="avatar">
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="32" height="32" rx="16" fill="#43F4EE"/>
                            <path d="M22.6668 23.5V21.8333C22.6668 20.9493 22.3156 20.1014 21.6905 19.4763C21.0654 18.8512 20.2176 18.5 19.3335 18.5H12.6668C11.7828 18.5 10.9349 18.8512 10.3098 19.4763C9.68469 20.1014 9.3335 20.9493 9.3335 21.8333V23.5" stroke="#00514E" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M15.9998 15.1667C17.8408 15.1667 19.3332 13.6743 19.3332 11.8333C19.3332 9.99238 17.8408 8.5 15.9998 8.5C14.1589 8.5 12.6665 9.99238 12.6665 11.8333C12.6665 13.6743 14.1589 15.1667 15.9998 15.1667Z" stroke="#00514E" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </span>
                        <span class="ml-2 d-none d-lg-block">
                            <span class="text-default">{{ Auth::user()->name }}</span>
                        </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                                <button class="dropdown-item" type="submit"><i class="dropdown-icon fe fe-log-out"></i> Выйти</button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <div class="bottom-block menu">
                <div class="container">
                    <div class="row">
                        <div class="dropdown">
                            <a href="#" class="name" data-toggle="dropdown">
                                <i class="fe fe-home"></i>
                                <span>Управление объектами</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        @include('partials.message')
            @else
                @include('partials.register_header')
            @endif
        @else
            @include('partials.register_header')
        @endif
    @else
        @include('partials.register_header')
    @endif
    @yield('content')
    </div>
<footer class="footer">
    <div class="container">
    <div class="row align-items-center flex-row-reverse">
        <div class="col-12 mt-3 mt-lg-0 text-center">
        Copyright © 2019. Все права защищены.
        </div>
    </div>
    </div>
</footer>
</div>
@yield('script')
  </body>
</html>
