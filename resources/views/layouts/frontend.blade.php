<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-equiv="Content-Language" content="{{ app()->getLocale() }}" />
    <meta name="msapplication-TileColor" content="#2d89ef" />
    <meta name="theme-color" content="#4188c9" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <title>@yield('title') Tourist Resource</title>
    <script src="{{asset('backend/js/vendors/feather-icons.min.js')}}"></script>
    <script src="{{asset('backend/js/vendors/feather.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables/datatables.css')}}">
    <link rel="stylesheet" href="{{ asset('css/styles.css')}}?ver=3">
    <script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js')}}"></script>
    <script src="{{ asset('backend/plugins/fullcalendar/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/vendors/daterangepicker.min.js') }}"></script>
</head>
    <body>
    <div class="wrapper" id="app">
        @yield('content')
        @include('front_partials.footer')
    </div>
    <script src="{{asset('js/vue.js')}}"></script>
    @yield('script')
    <script>
    let keyComponent;

    Vue.component('keyword', {
        data() {
            return {
                available: []
            }
        },
        template: `
            <div class="result keyword">
                <div class="result__box d-flex align-items-center justify-content-between" v-for="(item,key) in available" :key="key" v-on:click="setItem(item.name)">
                    <div class="left d-flex align-items-center">
                        <div class="image"><img :src="item.image[0]" alt="result"></div>
                        <div class="name">@{{ item.name }}</div>
                    </div>
                    <div class="right">
                        <div class="text">@{{ item.city.name_ru }} , @{{ item.city.country.name_ru }}</div>
                    </div>
                </div>
            </div>
        `,
        mounted(){
            keyComponent = this;
        },
        methods:{
            setItem: function(value){
                let changeInput = $('#keyword');
                changeInput.val(value);
                changeInput.focusout();
                $('.result').removeClass('show');
            },
            fetchByKeyword: function(value){
                let data = {
                    keyword : value
                };
                let app = this;
                $.ajax({
                    type: 'get',
                    url: '{{action('PageController@getByKeyWord')}}',
                    data: data,
                    success: function(msg){
                        app.available = msg;
                    }
                });
            },
        }
    });
        let app = new Vue({
            el: '#app',
            data: {
                basketCount : 0,
            },
            mounted(){
            },
            methods: {

            }
        });
        $(document).ready(function(){
          $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
            $('.search-form').submit(function (e) {
                if($('.date-input').val() == 'Check-in — Check out'){
                    e.preventDefault();
                }
            })
          });
            $('.nav-light__links.active').click(function () {
                $('.mob-view').toggleClass('open');
            });
        let adults = 1;
        let children = 0;
        let rooms = 1;
        @if(isset($information))
         adults = {{$information[0]}};
         children = {{$information[1]}};
         rooms = {{$information[2]}};
        @endif
        let childrenCounter = $('.children-count');
        let adultCounter = $('.adult-count');
        let roomCounter = $('.room-count');
        let mainInput = $('.adult-changer');
        $('.plus').on('click',function () {
            let counterClass = $(this).parent().find('.counter').data('count');
            let data = -1;
            if(counterClass == 'children-count' &&  children < 10) { children = children + 1; data = children;}
            if(counterClass == 'adult-count' &&  adults < 10) { adults = adults + 1; data = adults;}
            if(counterClass == 'room-count' &&  rooms < 10) { rooms = rooms + 1; data = rooms;}
            mainInput.val([adults,children, rooms]);
            if(data >= 0) $('.'+counterClass).html(data);
        });
        $('.minus').on('click',function () {
            let counterClass = $(this).parent().find('.counter').data('count');
            let data = -1;
            if(counterClass == 'children-count' &&  children > 0) { children = children - 1; data = children;}
            if(counterClass == 'adult-count' &&  adults > 1) { adults = adults - 1; data = adults;}
            if(counterClass == 'room-count' &&  rooms > 1) { rooms = rooms - 1; data = rooms;}

            if(data >= 0) $('.'+counterClass).html(data);
        });
        $('.adults').on('click',function () {
            $('.amount').toggleClass('show');
            $('.search__right').toggleClass('active');
            $('.modal-date').toggleClass('show');
        });
        let inputKeyword = $('#keyword');
        inputKeyword.focus(function () {
            let typingTimer;
            let doneTypingInterval = 1000;
            inputKeyword.keyup(function(){
                clearTimeout(typingTimer);
                if (inputKeyword.val().length > 0) {
                    typingTimer = setTimeout(function(){
                        keyComponent.fetchByKeyword(inputKeyword.val());
                    }, doneTypingInterval);
                }
            });
            $('.result').addClass('show');
            $(this).parent().addClass('active');
            $('.modal-date').toggleClass('show');
        });
        $('body').click(function(evt) {
            if (!($(evt.target).is('.keyword') || $(evt.target).parent().is('.keyword'))){
                if(($('.result').hasClass('show'))) $('.modal-date').removeClass('show');
                $('.result').removeClass('show');
                $('#keyword').parent().removeClass('active');
                return true;
            }
               else return false;
        });
        $('body').click(function(evt) {
            if (!($(evt.target).is('.adult-changer') || $(evt.target).is('.amount__box-button') || $(evt.target).is('.amount') || $(evt.target).parent().is('.amount') || $(evt.target).parent().parent().is('.amount'))){
                    if(($('.amount').hasClass('show'))) $('.modal-date').removeClass('show');
                    $('.amount').removeClass('show');
                    $('.search__right').removeClass('active');
                return true;
            }
               else return false;

        });
        })
        feather.replace();
    </script>
    </body>
</html>
