@extends('layouts.backend')

@section('content')
@include('partials.header')
<section>
		<div class="container">
			<div class="row">
				<h2 class="blue-title">{{ \Carbon\Carbon::parse($checkin_date)->format('F d, Y')}} — {{ \Carbon\Carbon::parse($checkout_date)->format('F d, Y')}}</h2>
                <form action="{{ action('BookingController@create', $id) }}" method="GET" autocomplete="off">
                    <input type="hidden" name="from" value="{{ $checkin_date }}">
                    <input type="hidden" name="to" value="{{ $checkout_date }}">
					<div class="white-block">
						<ul class="list-block bron_list">
							<li class="item item_head">
								<span>Название</span>
								<span>Свободных</span>
								<span>Выбрать</span>
							</li>
                            @foreach($rooms as $room)
                                @if(($room->room_count - $booking->where('room_id',$room->id)->count() > 0))
                                <li class="item">
								<div class="left-block">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>
                                    <p>{{ $room->room_type()->first()->name_ru }} {{ $room->room_name()->first()->name_ru }}</p>
                                    <input type="hidden" name="room[]" value="{{ $room->id }}">
								</div>
								<div class="middle-block">
									{{ $room->room_count - $booking->where('room_id',$room->id)->count()  }}
								</div>
								<div class="right-block long input_number_block">
									<button type="button" class="btn_plus counter-button up_count" title='Down'><i class="fe fe-plus"></i></button>
                                      <input class='input_number counter' style="pointer-events:none;" name="count[]" data-maximum="{{ $room->room_count - $booking->where('room_id',$room->id)->count()  }}" type="text" value='0' />
									<button type="button" class="btn_minus counter-button down_count" title='Up'><i class="fe fe-minus"></i></button>
								</div>
							</li>
                            @endif
                            @endforeach
						</ul>
					</div>
					<div class="button-block">
						<button type="submit" class="btn font-weight-normal continue-btn">Продолжить</button>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection
@section('script')
<script>
let __origDefine = define;
define = null;
</script>
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script>
$(document).ready(function(){
    $('.counter-button').click(function(e){
        e.preventDefault();
        let counter = $(this).parent().parent().find('.counter');
        let button_classes, value = +counter.val();
        let maximum = +counter.data('maximum');
        button_classes = $(e.currentTarget).prop('class');
            if(button_classes.indexOf('up_count') !== -1){
                value = (value) + (value < maximum ? 1 : 0);
            } else {
                value = (value) - 1;
            }
            value = value < 0 ? 0 : value;
            counter.val(value);
    });
    // $('.counter').click(function(){
    //     $(this).focus().select();
    // });
});
</script>
@endsection
