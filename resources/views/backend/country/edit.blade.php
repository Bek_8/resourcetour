@extends('layouts.backend')

@section('content')
@include(Auth::user()->hasRole('admin') ? 'partials.admin_header':'partials.header')
<div class="my-3 my-md-5">
    <div class="container">
            <div class="card">
                <div class="card-header">Изменить город</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('CountryController@update', $data->id) }}" method="POST" class="reg-form" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name">Название (RU)</label>
                            <input value="{{ $data->name_ru }}" required="required" name="name_ru" type="text" class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="desc">Название (UZ)</label>
                            <input value="{{ $data->name_en }}" required="required" name="name_en" type="text" class="form-control" id="desc">
                        </div>
                    <button class="continue-form mt-5 mb-5">Сохранить</button>
                    </form>
                </div>
            </div>
    </div>
</div>
@endsection
