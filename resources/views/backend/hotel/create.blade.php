@extends(Auth::user()->hotels()->first() ? 'layouts.backend': 'layouts.app')

@section('content')
@if(Auth::user()->hotels()->first())
    @include('partials.hotel_header')
<div class="container">
    <h2 class="blue-title">Новый объект</h2>
</div>
@endif
<section class="jv_hotel_create_page">
<div class="container">
<div class="row">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <form action="{{ action('HotelController@store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="white-block mb-30">
            <div class="head">
                <h3>Тип объекта</h3>
            </div>
            <div class="content">
                <div class="object-block">
{{--                    <div class="item">--}}
{{--                        <img src="{{ asset('backend/images/hotel.jpg')  }}" alt="" />--}}
{{--                        <input id="hotel" type="radio" name="type_id" value="1" checked="checked" />--}}
{{--                        <label for="hotel"><span>Отель</span></label>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <img src="{{ asset('backend/images/hostel.jpg')  }}" alt="" />--}}
{{--                        <input id="hostel" type="radio" name="type_id" value="2" />--}}
{{--                        <label for="hostel"><span>Хостел</span></label>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <img src="{{ asset('backend/images/house.jpg')  }}" alt="" />--}}
{{--                        <input id="house" type="radio" name="type_id" value="3" />--}}
{{--                        <label for="house"><span>Гостевой дом</span></label>--}}
{{--                    </div>--}}
{{--                    <div class="item">--}}
{{--                        <img src="{{ asset('backend/images/apartments.jpg')  }}" alt="" />--}}
{{--                        <input id="aparataments" type="radio" name="type_id" value="4" />--}}
{{--                        <label for="aparataments"><span>Апартаменты</span></label>--}}
{{--                    </div>--}}
                    @foreach( $type as $key => $datas )
                    <div class="item">
                        <img src="{{ asset('uploads/type/'.$datas->id.'.jpg')}}" alt="" />
                        <input id="hotel{{$datas->id}}" type="radio" name="type_id" value="{{ $datas->id }}" @if($key == 0) checked="checked" @endif />
                        <label for="hotel{{$datas->id}}"><span>{{ $datas->text_ru }}</span></label>
                    </div>
                    @endforeach
                </div>
            </div>
      </div>
        <div class="white-block mb-30">
          <div class="head">
              <h3>Об объекте</h3>
          </div>
          <div class="content">
            <div class="input-block">
                <div class="input">
                    <label for="name">Название</label>
                    <input required="required" type="text" name="name" value="{{old('name')}}" class="form-control regStepOne" id="name" placeholder="" />
                </div>
              </div>
              <div class="input-block">
                <div class="input">
                    <label>Страна:</label>
                    <select name="country" id="select-countries" class="form-control custom-select">
                        @foreach( $country as $datas )
                        <option data-data='{"image": "/backend/images/flags/{{$datas->flag}}.svg"}' value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                        @endforeach
                    </select>
                </div>
              </div>
              <div class="input-block">
                <div class="input">
                    <label>Город</label>
                    <select name="city_id" class="form-control" id="select-beast">
                        @foreach( $data as $datas )
                        <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                        @endforeach
                    </select>
                </div>
              </div>
                <div class="input-block">
                    <div class="input">
                        <label for="address">Адрес</label>
                        <input required="required" type="text" value="{{old('address')}}" class="regStepTwo" name="address" id="addressHotel" />
                    </div>
                    <div class="text-block max-992">
                        <p>укажите адрес, используя латинский алфавит</p>
                    </div>
                </div>
                @if(Auth::user()->hasRole('admin'))
                <div class="form-group">
                    <label for="manager">Менеджер</label>
                    <select name="user_id" class="form-control" id="manager">
                        @foreach( $user as $key => $datas )
                            @if($key != 0)
                        <option value="{{ $datas->id }}">
                            {{ $datas->name }}
                        </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                @endif
                @if(Auth::user()->hasRole('manager'))
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                @endif
                <div class="input-block">
                    <div class="input">
                        <label for="service-phone">Телефон отдела обслуживания </label>
                        <div class="phone">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone phone-icon"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>
                            <input required="required" type="tel" class="regStepTwo forPhone" name="phone" id="phoneHotel">
                        </div>
                    </div>
                    <div class="text-block max-992">
                        <p>с кодом страны, города или оператора</p>
                    </div>
                </div>
                <div class="input-block pb-3 jv_map_register">
                    <div class="input">
                        <label>Локация:</label>
                        <div id="map" style="height: 400px; width: 100%;"></div>
                        <input id="long" value="69.240562" name="long" type="hidden" class="form-control">
                        <input id="lat" value="41.311081" name="lat" type="hidden" class="form-control">
                    </div>
                </div>
          </div>
        </div>
        <div class="white-block mb-30">
            <div class="head">
                <h3>Правила</h3>
            </div>
            <div class="content secondary">
                <div class="input-block">
                    <div class="input half">
                        <p>Регистрация заезда</p>
                          <select name="income" id="select-beast1" class="form-control custom-select">
                                @foreach ($times as $time)
                                    <option value="{{$time->format('H:i')}}">с {{$time->format('H:i')}}</option>
                                @endforeach
                                <option value="00:00">с 00:00</option>
                          </select>
                          <select name="income_to" id="select-beast2" class="form-control custom-select">
                                @foreach ($times as $time)
                                    <option value="{{$time->format('H:i')}}">до {{$time->format('H:i')}}</option>
                                @endforeach
                                <option value="00:00">до 00:00</option>
                          </select>
                    </div>
                </div>
                <div class="input-block">
                    <div class="input half">
                        <p>Регистрация отъезда</p>
                            <select name="outcome" id="select-beast3" class="form-control custom-select">
                                @foreach ($times as $time)
                                    <option value="{{$time->format('H:i')}}">с {{$time->format('H:i')}}</option>
                                @endforeach
                                <option value="00:00">с 00:00</option>
                            </select>
                            <select name="outcome_to" id="select-beast4" class="form-control custom-select">
                                @foreach ($times as $time)
                                    <option value="{{$time->format('H:i')}}">до {{$time->format('H:i')}}</option>
                                @endforeach
                                <option value="00:00">до 00:00</option>
                            </select>
                    </div>
                </div>
            </div>
            <div class="content secondary">
                <div class="input-block">
                    <div class="input">
                        <p>Документы, обязательные при регистрации</p>
                        <div>
                          <label class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" name="confirmation" value="1" />
                            <span class="custom-control-label">Удостоверение личности с фотографией</span>
                          </label>
                          <label class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" name="credit" value="1" />
                            <span class="custom-control-label">Кредитная карта</span>
                          </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content secondary">
                <div class="input-block">
                    <div class="input">
                        <p>Карты, принимаемые к оплате</p>
                        <div>
                          <label class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" name="cards[]" value="mastercard" >
                            <span class="custom-control-label">Mastercard</span>
                          </label>
                          <label class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" name="cards[]" value="visa">
                            <span class="custom-control-label">Visa</span>
                          </label>
                          <label class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" name="cards[]" value="unionpay">
                            <span class="custom-control-label">UnionPay</span>
                          </label>
                          <label class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" name="cards[]" value="jcb">
                            <span class="custom-control-label">JCB</span>
                          </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content secondary">
                <div class="input-block">
                    <div class="input">
                        <p>Разрешается ли брать с собой животных?</p>
                        <div>
                          <label class="custom-control custom-radio custom-control-inline">
                              <input type="radio" class="custom-control-input" name="animals" value="0" checked="">
                              <span class="custom-control-label">Не разрешается</span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                              <input type="radio" class="custom-control-input" name="animals" value="1">
                              <span class="custom-control-label">Разрешается</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="white-block mb-30">
                <div class="head">
                    <h3>Предоставляемые услуги и удобства</h3>
                </div>
                <div class="accordion-content">
                    <div class="head-accordion jv_accordion_head">
                        <p>Укажите предоставляемые на территории отеля услуги и удобства. Отметье, если они оплачиваются отдельно.</p>
                        <div class="bottom-text">
                            <span>Тип услуг</span>
                            <span>Оплачивается отдельно</span>
                        </div>
                    </div>
                </div>

                  <div class="card-body-form">
                      <div class="accordion" id="accordion">
                        @foreach($service as $item)
                          <div class="card">
                            <div class="card-header">
                              <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#acc{{$item->id}}" aria-expanded="false" aria-controls="{{$item->id}}">
                                    <span><i class="fe fe-plus"></i></span> {{ $item->name_ru }}
                                </button>
                              </h2>
                            </div>
                            <div id="acc{{$item->id}}" class="collapse" aria-labelledby="heading{{$item->id}}" data-parent="#accordion">
                              <div class="card-body p-0">
                                    @foreach($item->inners as $inner)
                                        <div class="itemServices checkboxes">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input action" name="service[]" value="[0,{{$inner->id}}]" data-info="{{ $inner->id }}" />
                                                <span class="custom-control-label">{{ $inner->name_ru }}</span>
                                            </label>
                                            <label class="custom-switch">
                                                <input type="checkbox" value="1" class="custom-switch-input changer">
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                          </div>
                        @endforeach
                        </div>
                  </div>
        </div>
        <div class="white-block mb-30">
        <div class="head">
            <h3>Общие фотографии</h3>
        </div>
        <div class="card-header-after">
          <p class="jv_file_uploader_text pt-3">Добавьте общие фотографии объекта, экстерьер и интерьер помещений. Мин. размер изображений 945x630 пикс.</p>
          <div class="fotoUploader">
              <input required="required" type="file" name="image[]" class="filer_input2" multiple="multiple">
          </div>
        </div>
        </div>
        <div class="button-block">
            <button type="submit" class="continue-btn">Продолжить</button>
        </div>
    </form>
</div>
</div>
</section>
@endsection
@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&callback=initMap"></script>
<script>
let checkboxes = $('.checkboxes');
checkboxes.each(function() {
        let action = $(this).find('.action');
        let changer = ($(this).find('.changer'));
        changer.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
        });
        action.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
        });
});

let latitude = $('#lat');
let longitude = $('#long');

function initMap() {
let latlng = new google.maps.LatLng(41.311081, 69.240562);
let map = new google.maps.Map(document.getElementById('map'), {
    center: latlng,
    zoom: 12,
    animation:google.maps.Animation.BOUNCE
});
let marker = new google.maps.Marker({
    position: latlng,
    map: map,
    draggable: true
});
let lat ,long;
google.maps.event.addListener(marker, 'dragend', function (event) {
    lat  = this.getPosition().lat().toFixed(6);
    long = this.getPosition().lng().toFixed(6);
    // console.log(lat + 'lat' + long + 'lng')
    latitude.val(lat);
    longitude.val(long);
});
}
</script>
@endsection
