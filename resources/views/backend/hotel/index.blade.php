@extends('layouts.backend')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
        <h3 class="card-title">Продукты</h3>
         <a class="btn btn-sm btn-outline-success" href="{{ action('HotelController@create') }}">Добавить</a>
        </div>
        <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
            <thead>
            <tr>
                <th>№</th>
                <th>Название</th>
                <th>Категория</th>
                <th>Фотография</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datas)
            <tr>
                <td>#{{ $datas->id }}</td>
                <td>{{ $datas->name }}</td>
                <td>{{ $datas->category->name }}</td>
                <td>
                    <img src="{{ asset('uploads/products/'.$datas->id.'.jpg') }}" width="140">
                </td>
                <td class="text-center">

                    <a href="{{ action('HotelController@edit' , $datas->id) }}" class="btn btn-primary">
                        <i class="fe fe-edit"></i>
                    </a>
                    <a href="{{ action('HotelController@edit' , $datas->id) }}" class="btn btn-primary">
                        Включить
                    </a>
                    <form class="d-inline-block" action="{{ action('HotelController@delete' , $datas->id) }}" onclick="return confirm('Вы уверены?')" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">
                        <i class="fe fe-trash"></i>
                    </button>
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection
