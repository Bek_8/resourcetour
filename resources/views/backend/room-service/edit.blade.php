@extends('layouts.backend')

@section('content')
@include(Auth::user()->hasRole('admin') ? 'partials.admin_header':'partials.header')
<div class="my-3 my-md-5">
    <div class="container">
        <div class="card">
            <div class="card-header">Изменить</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{ action('RoomServiceController@update',$id) }}" class="reg-form" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div>
                        <div class="form-group">
                            <label for="name">Название (RU)</label>
                            <input value="{{ $data->name_ru }}" name="name_ru" type="text" class="form-control" id="name" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="name">Название (EN)</label>
                            <input value="{{ $data->name_en }}" name="name_en" type="text" class="form-control" id="name" placeholder="Введите название">
                        </div>
                        <div class="form-group pt-5">
                            <div class="d-flex align-items-center">
                                <img src="{{ asset('uploads/service/'.$id.'.jpg') }}" alt="" width="264" class="mb-5" />
                                <span class="pl-5">Загрузите для изменения</span>
                            </div>
                            <input type="file" class="form-control-file" name="image" />
                        </div>
                    </div>
                    <button class="continue-form mt-5 mb-5">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
