@extends('layouts.backend')

@section('content')
@include('partials.header')
	<section>
		<div class="container">
			<div class="row">
                <form action="{{ action('SaleController@update', $id) }}" method="POST" id="form">
                @csrf
                @method('PUT')
                <div class="white-block mb-30">
                    <div class="head">
                        <h3>
                            <i class="fa-lg fe fe-percent pr-2"></i>
                            Установить скидку
                        </h3>
                        <p class="pt-20">
                            Привлейкате больше клиентов, устанавливая скидки. Ваш объект будет отмечен специальным стикером. Скидка распространяются сразу на все номера. Вы всегда можете отключить действие скидки.
                        </p>
                    </div>
                    <div class="content secondary">
                        <div class="block">
                        <div class="selectgroup selectgroup-pills">
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="0" class="selectgroup-input" @if($data->sale == 0) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">0%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="10" class="selectgroup-input" @if($data->sale == 10) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">10%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="20" class="selectgroup-input" @if($data->sale == 20) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">20%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="30" class="selectgroup-input" @if($data->sale == 30) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">30%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="40" class="selectgroup-input" @if($data->sale == 40) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">40%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="50" class="selectgroup-input" @if($data->sale == 50) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">50%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="60" class="selectgroup-input" @if($data->sale == 60) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">60%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="70" class="selectgroup-input" @if($data->sale == 70) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">70%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="80" class="selectgroup-input" @if($data->sale == 80) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">80%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                              <label class="selectgroup-item custom-sale">
                                <input type="radio" name="sale" value="90" class="selectgroup-input" @if($data->sale == 90) checked @endif>
                                <span class="selectgroup-button selectgroup-button-icon">90%</span>
                                <p class="chosen">Выбрано</p>
                              </label>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="button-block">
                    <button type="submit" class="continue-btn">Активировать скидку</button>
                </div>
                </form>
            </div>
		</div>
	</section>
@endsection
