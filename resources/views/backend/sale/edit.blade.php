
@extends('layouts.backend')

@section('content')
{{--@include('partials.header')--}}
<div class="my-3 my-md-5">
    <div class="container">
        <form action="{{ action('RoomController@update', $data->id) }}" class="reg-form row justify-content-center" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="lastStep_1">
              <h5 class="j_title">Добавьте один или несколько номеров, чтобы ваш новый объект появился на сайте</h5>
            <div class="card">
                <div class="card-header head-first1">
                    <h3 class="card-title">Номер</h3>
                </div>
                <div class="card-body-form">
                    <div class="form-group">
                      <label class="form-label" for="select_type_number">Выберите тип номера:</label>
                        <select name="room_name_id" disabled="disabled" id="select_type_number" class="custom-select j_select">
                                <option value="{{ $data->id }}">{{ $data->room_name->name_ru }}</option>
                        </select>
                    </div>
                    <div class="j_block">
                      <div class="form-group">
                        <label class="form-label" for="select_name_number">Названние номера</label>
                        <select name="room_type_id" disabled="disabled" id="select_name_number" class="custom-select j_select">
                            <option value="{{ $data->id }}">{{ $data->room_type->name_ru }}</option>
                        </select>
                      </div>
                      <p>Название  будет отображаться на странице отеля</p>
                    </div>
                    <div class="form-group j_form_group">
                        <div class="j_input_group">
                            <label class="form-label">Кол-во номеров данного типа</label>
                            <input required="required" disabled type="number" name="room_count" value="{{$data->room_count}}" class="form-control" min="1" id="internalNumber">
                        </div>
                        <div class="j_input_group">
                            <label class="form-label">Максимальная вместимость чел.</label>
                            <input required="required" disabled type="number" name="capacity" value="{{$data->capacity}}" class="form-control" min="1" id="personNumber">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card" id="personNumberPriceBlock">
                <div class="card-header head-first1">
                    <h3 class="card-title">Укажите стоимость размещения с учетом всех налогов</h3>
                </div>
                <div class="card-body-form">
                    <div class="form-group j_form_group">
                        <div class="j_input_group j_usd_input">
                            <label class="form-label">Стоимость размещения 1 чел.</label>
                            <div class="input-group">
                                <span class="input-group-prepend" id="basic-addon1">
                                  <span class="input-group-text">USD</span>
                                </span>
                                <input required="required" type="text" name="price[]" value="0.00" class="form-control" data-mask="000.000.000.000.000,00" data-mask-reverse="true" autocomplete="off" maxlength="22">
                              </div>
                        </div>
                    </div>
                </div>
              </div>
            <div class="card" id="numerationBlock">
            <div class="card-header head-first1">
                <h3 class="card-title">Внутренняя нумерация</h3>
            </div>
            <p class="j_sub_title">Если указать внутреннюю нумерацию, вы сможете размещать гостей согласно желаемому порядковму номеру.</p>
            <div class="card-body-form">
                <div class="form-group j_form_group">
                    <div class="j_input_group j_usd_input">
                        <div class="input-group">
                            <input required="required" type="number" name="field-name"  class="form-control" disabled >
                        </div>
                    </div>
                    <div class="j_input_group j_usd_input">
                        <div class="input-group">
                            <input required="required" type="number" name="field-name"  class="form-control" >
                        </div>
                    </div>
                </div>
            </div>
            </div>
                <div class="card" id="badsBlock">
                <div class="card-header head-first1">
                    <h3 class="card-title">Наличие кроватей</h3>
                </div>
                <div class="card-body-form">
                    @foreach(json_decode($data->bed,true) as $key => $checked)
                    <div class="form-group j_form_group">
                        <div class="j_input_group">
                            <label class="form-label">Тип кровати</label>
                            <select  class="custom-select j_select" name="bed[]">
                                @foreach($bed as $item)
                                    <option value="{{$item->id}}" @if($item->id == $checked) selected @endif>{{ $item->name_ru  }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="j_input_group">
                            <label class="form-label">Количество</label>
                            <input required="required" type="number" value="{{json_decode($data->bed_count,true)[$key]}}" name="bed_count[]" class="form-control" min="0">
                        </div>
                    </div>
                    @endforeach
                    <div class="form-group j_form_group pb-5">
                        <button type="button" class="btn btn-dribbble" id="btnAddBads"><i class="fe fe-plus-circle mr-2"></i>Добавить кровать</button>
                    </div>
                </div>
              </div>
        <div class="card">
          <div class="card-header head-first1">
              <h3 class="card-title">Предоставляемые услуги и удобства</h3>
          </div>
          <div class="card-header-after">
              <p>Укажите предоставляемые на территории отеля услуги и удобства. Отметье, если они оплачиваются отдельно.</p>
              <h6>Тип услуг <span>Оплачивается отдельно</span></h6>
          </div>

          <div class="card-body-form">
              <div class="accordion" id="accordion">
                @foreach($service as $item)
                  <div class="card">
                    <div class="card-header">
                      <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#acc{{$item->id}}" aria-expanded="false" aria-controls="{{$item->id}}">
                            <span><i class="fe fe-plus"></i></span> {{ $item->name_ru }}
                        </button>
                      </h2>
                    </div>
                    @foreach($item->inners as $inner)
                    <div id="acc{{$item->id}}" class="collapse" aria-labelledby="heading{{$inner->id}}" data-parent="#accordion">
                      <div class="card-body">
                        <div class="itemServices checkboxes">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input action" name="service[]" @if(!empty(json_decode($data->services,true)[$inner->id])) @if(json_decode($data->services,true)[$inner->id] > 0) checked  value="[{{json_decode($data->services,true)[$inner->id]}},{{$inner->id}}]" @endif @else value="[0,{{$inner->id}}]" @endif data-info="{{ $inner->id }}" />
                                <span class="custom-control-label">{{ $inner->name_ru }}</span>
                            </label>
                            <label class="custom-switch">
                                <input type="checkbox" value="1" @if(!empty(json_decode($data->services,true)[$inner->id])) @if(json_decode($data->services,true)[$inner->id] > 1) checked @endif @endif class="custom-switch-input changer">
                                <span class="custom-switch-indicator"></span>
                            </label>
                        </div>
                        </div>
                    </div>
                    @endforeach
                  </div>
                @endforeach
                </div>
          </div>
        </div>
          <div class="card">
            <div class="card-header head-first1">
                <h3 class="card-title">Общие фотографии</h3>
            </div>
            <div class="card-header-after">
              <p>Добавьте общие фотографии объекта, экстерьер и интерьер помещений. Мин. размер изображений 945x630 пикс.</p>
              <div class="fotoUploader">
                <ul class="jFiler-items-list jFiler-items-grid">
                    @foreach($images as $image)
                    <li class="gallryUploadBlock_item photo-thumbler" data-image="{{basename($image)}}">
                        <div class="jFiler-item-thumb-image">
                            <img src="{{asset($image)}}" alt="image">
                        </div>
                        <div class="removeItem">
                            <span class="deletePhoto" data-image="{{basename($image)}}">
                                <i class="fe fe-minus"></i>
                            </span>
                        </div>
                    </li>
                    @endforeach
                </ul>
                  <input type="file" name="image[]" class="filer_input3" multiple="multiple" />
              </div>
            </div>
          </div>
          </div>
            <button class="continue-form" type="submit">Сохранить</button>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>
let personNumberInput = $('#personNumber');
let personNumberPriceBlock = $('#personNumberPriceBlock');
let internalNumber = $('#internalNumber');
let numerationBlock = $('#numerationBlock');
const pasteElementInput = (index, blockName , value) =>{
    if(blockName =='personNumber'){
      return (`<div class="j_input_group j_usd_input">
          <label class="form-label">Стоимость размещения ${index} чел.</label>
          <div class="input-group">
            <span class="input-group-prepend" id="basic-addon1">
              <span class="input-group-text">USD</span>
            </span>
            <input type="text" name="price[]" value="${value}" required="required" class="form-control" data-mask="000.000.000.000.000,00" data-mask-reverse="true" autocomplete="off" maxlength="22">
          </div>
        </div>`
      )
    }else{
      return (`<div class="j_input_group j_usd_input">
                  <div class="input-group">
                      <input type="number" name="numeration[]"  class="form-control" value="${index}" disabled >
                  </div>
              </div>
              <div class="j_input_group j_usd_input">
                  <div class="input-group">
                      <input type="number" required="required" value="${value}" name="numeration[]"  class="form-control" >
                  </div>
              </div>`
      )
    }
}
personNumberPriceBlock.hide()
numerationBlock.hide()

$(document).ready(()=>{
$(".filer_input3").filer({
  limit: null,
  maxSize: null,
  extensions: null,
  changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
  showThumbs: true,
  theme: "dragdropbox",
  templates: {
    box: '<ul class="jFiler-items-list buttonAdder jFiler-items-grid"></ul>',
    item: `<li class="gallryUploadBlock_item jFiler-item">
    {{'{{fi-image}'.'}'}}
    <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
    </li>`,
    progressBar: '<div class="bar"></div>',
    itemAppendToEnd: true,
    canvasImage: true,
    removeConfirmation: false,
    _selectors: {
      list: '.jFiler-items-list',
      item: '.jFiler-item',
      progressBar: '.bar',
      remove: '.fe.fe-minus'
    }
  },
  dragDrop: {
    dragEnter: null,
    dragLeave: null,
    drop: null,
    dragContainer: null,
  },
  files: null,
  addMore: true,
  allowDuplicates: false,
  clipBoardPaste: true,
  excludeName: null,
  beforeRender: null,
  afterRender: null,
  beforeShow: null,
  beforeSelect: null,
  itemAppendToEnd: true,
  onSelect: null,
  afterShow: function(jqEl, htmlEl, parentEl, itemEl){
    $('.galleryAddElement').hide()
    $('.cloneGalleryAddElement').remove()
    $('.buttonAdder').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
  },
  onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
    let filerKit = inputEl.prop("jFiler"),
          file_name = filerKit.files_list[id].name;
      if(filerKit.files_list.length == 1){
        $('.galleryAddElement').show()
      }
  },
  onEmpty: null,
  options: null,
});
$('.uploader').on('click', '.cloneGalleryAddElement', function(e){
  $('.galleryAddElement').trigger('click');
});
$('.deletePhoto').click((e) =>{
    let removeImage = $(e.target);
    let imageData = removeImage.data('image');
    let deleteItem = $.get("{{action('RoomController@removeImage',$id)}}" , {file_name: imageData});
    deleteItem.done(()=>{
        removeImage.parent().parent().remove();
    });
});
      let person = personNumberInput;
      let personVal = person.val();
      let data = JSON.parse(@json($data->price));
      $('#personNumberPriceBlock .j_form_group').empty();
      if(personVal != '' && personVal >= 1){
        for (let index = 0; index < personVal; index++) {
          $('#personNumberPriceBlock .j_form_group').append(pasteElementInput(index+1, 'personNumber',(data[index] !== undefined ? data[index] : ' ')))
        }
        personNumberPriceBlock.show()
      }else{
        personNumberPriceBlock.hide()
      }
  let numeration = internalNumber;
  let numerationVal = numeration.val();
  let numerationData = JSON.parse(@json($data->numeration));
  $('#numerationBlock .j_form_group').empty();
  if(numerationVal != '' && numerationVal >= 1){
    for (let index = 0; index < numerationVal; index++) {
      $('#numerationBlock .j_form_group').append(pasteElementInput(index+1, 'internalNumber',(numerationData[index] !== undefined ? numerationData[index] : ' ')));
    }
    numerationBlock.show();
  }else{
    numerationBlock.hide();
  }
});
let btnAddBads = $('#btnAddBads');
let btnRemoveBad = $('.btnRemoveBad');
let elementBadHtml = `<div class="form-group j_form_group">
                    <div class="j_input_group">
                        <label class="form-label">Тип кровати</label>
                        <select  class="custom-select j_select" name="bed[]">
                            @foreach($bed as $item)
                                <option value="{{ $item->id  }}">{{ $item->name_ru  }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="j_input_group">
                        <label class="form-label">Количество</label>
                        <input required=required" type="number" name="bed_count[]" class="form-control" min="0">
                    </div>
                    <a href="#" class="btnRemoveBad"><i class="fe fe-trash-2"></i></a>
                </div>`
btnAddBads.on('click', function() {
  $(this).parent().before(elementBadHtml)
})
$('#badsBlock').on('click', '.btnRemoveBad', function(e) {
  e.preventDefault();
  $(this).parent('.j_form_group').remove();
})

$('.btn_saveOrFinish').on('click', function(e) {
  e.preventDefault();
})
let checkboxes = $('.checkboxes');
checkboxes.each(function() {
        let action = $(this).find('.action');
        let changer = ($(this).find('.changer'));
        changer.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
        });
        action.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
        });
});
</script>
@endsection
