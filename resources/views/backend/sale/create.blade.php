@extends(Auth::user()->hotels()->first()->rooms()->first() ? 'layouts.backend': 'layouts.app')

@section('content')
@if(Auth::user()->hotels()->first()->rooms()->first())
    @include('partials.header')
@endif
<section>
    <div class="container">
        <div class="row">
        <form action="{{ action('RoomController@store', $id) }}" method="POST" enctype="multipart/form-data" id="form">
            @csrf
              <h1 class="blue-title">Добавьте один или несколько номеров, чтобы ваш новый объект появился на сайте</h1>
                <div class="white-block mb-30">
                    <div class="head">
                        <h3>Номер</h3>
                    </div>
                    <div class="content secondary">
                        <div class="input-block">
                        <div class="input">
                          <p>Выберите тип номера:</p>
                            <select name="room_type_id" id="select_type_number" class="custom-select">
                            <option value="0">Выбрать</option>
                            @foreach($room_type as $item)
                                <option value="{{ $item->id }}">{{ $item->name_ru  }}</option>
                            @endforeach
                            </select>
                        </div>
                        </div>
                          <div class="input-block">
                            <div class="input">
                            <p>Название номера</p>
                            <select name="room_name_id" id="select_name_number" class="custom-select">
                            <option value="0" data-value="0">Выбрать</option>
                                @foreach($name as $item)
                                    <option value="{{ $item->id }}" data-value="{{$item->type_id}}">{{ $item->name_ru  }}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="text-block max-992">
                              <p>
                                Название будет отображаться на странице отеля
                              </p>
                            </div>
                          </div>
                        <div class="input-block">
                            <div class="input half">
                                <div class="half-block max-992 mobile-mb-30">
                                    <p>Кол-во номеров данного типа</p>
                                    <input required="required" type="number" name="room_count" class="form-control" min="1" max="100" id="internalNumber">
                                </div>
                                <div class="half-block max-992">
                                    <p>Максимальная вместимость чел.</p>
                                    <input required="required" type="number" name="capacity" class="form-control" min="1" max="20" id="personNumber">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="white-block mb-30" id="personNumberPriceBlock">
                <div class="head">
                    <h3>Укажите стоимость размещения с учетом всех налогов</h3>
                </div>
                <div class="content secondary d-flex">
                    <div class="input-block inner col-lg-8 px-0 d-flex flex-wrap"></div>
                </div>
                </div>
                <div class="white-block mb-30" id="internalNumberBlock">
                <div class="head">
                    <h3>Внутренняя нумерация</h3>
                </div>
                <div class="content secondary">
                    <p class="content-text mb-30">Если указать внутреннюю нумерацию, вы сможете размещать гостей согласно желаемому порядковму номеру.</p>
                        <ul class="radio-switch" id="numeration_enabled">
								<li class="item">
									<input type="radio" id="radio1" name="numeration_enabled" value="0" checked="">
									<label for="radio1">Не указывать</label>
								</li>
								<li class="item">
									<input type="radio" id="radio2" name="numeration_enabled" value="1">
									<label for="radio2">Указать</label>
								</li>
							</ul>
                    <div>
                        <div class="numeration"></div>
                    </div>
                </div>
                </div>
                <div class="white-block mb-30 changable" id="badsBlock">
                <div class="head">
                    <h3>Наличие кроватей</h3>
                </div>
                    <div class="content">
                        <div class="input-block with-trash">
                            <div class="input half">
                                <div class="half-block max-992 mobile-mb-30">
                                    <p>Тип кровати</p>
                                    <select  class="form-control custom-select h-auto" name="bed[]">
                                        @foreach($bed as $item)
                                            <option value="{{$item->id}}">{{ $item->name_ru  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="half-block max-992">
                                    <p class="second">Количество</p>
                                    <input required="required" type="number" value="1" name="bed_count[]" class="form-control w-100" min="1" />
                                </div>
                            </div>
                        </div>
                        <div class="input-block with-trash text-block">
                            <span class="add-button d-flex align-items-center" style="height: 42px;" id="btnAddBads"><i class="fe fe-plus-circle"></i> <span>Добавить кровать</span></span>
                        </div>
                    </div>
                </div>
                <div class="white-block mb-30 changable">
                <div class="head">
                    <h3>Предоставляемые услуги и удобства</h3>
                </div>
                <div class="accordion-content">
                    <div class="head-accordion">
                        <p>Укажите прилагаемые к номеру услуги и удобства. Отметье, если они оплачиваются отдельно.</p>
                        <div class="bottom-text">
                            <span>Тип услуг</span>
                            <span>Оплачивается отдельно</span>
                        </div>
                    </div>
                </div>

                  <div class="card-body-form">
                      <div class="accordion" id="accordion">
                        @foreach($service as $item)
                          <div class="card">
                            <div class="card-header">
                              <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#acc{{$item->id}}" aria-expanded="false" aria-controls="{{$item->id}}">
                                    <span><i class="fe fe-plus"></i></span> {{ $item->name_ru }}
                                </button>
                              </h2>
                            </div>
                            @foreach($item->inners as $inner)
                            <div id="acc{{$item->id}}" class="collapse" aria-labelledby="heading{{$inner->id}}" data-parent="#accordion">
                              <div class="card-body">
                                <div class="itemServices checkboxes">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input action" name="service[]" value="[0,{{$inner->id}}]" data-info="{{ $inner->id }}" />
                                        <span class="custom-control-label">{{ $inner->name_ru }}</span>
                                    </label>
                                    <label class="custom-switch">
                                        <input type="checkbox" value="1" class="custom-switch-input changer">
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                </div>
                            </div>
                            @endforeach
                          </div>
                        @endforeach
                        </div>
                  </div>
                </div>
          <div class="white-block mb-30 changable">
            <div class="head">
                <h3>Фотографии номера</h3>
            </div>
            <div class="card-header-after">
              <p class="pt-3">Добавьте фотографии номера</p>
              <div class="fotoUploader">
                  <input required="required" type="file" name="image[]" class="filer_input2" multiple="multiple">
              </div>
            </div>
          </div>
            <div class="button-block changable pb-5 reg-form">
                <button class="continue-btn" name="confirm" value="end" type="submit">Добавить номер</button>
{{--                <button class="blue-text ml-40" name="confirm" value="end" type="submit">Сохранить и завершить регистрацию</button>--}}
            </div>
        </form>
    </div>
    </div>
</section>
@endsection

@section('script')
<script>
let typeNumber = $('#select_type_number');
let nameNumber = $('#select_name_number');
let changable = $('.changable');
let numeration = $('.numeration');
let numerationSwitch = $("#numeration_enabled input",'#form');
$(document).ready(()=>{
    nameNumber.attr('disabled',true);
    changable.hide();
    numeration.hide()
});
numerationSwitch.change(()=>{
    let checked = $('#numeration_enabled input').filter(":checked").val();
    (checked == 0 ? numeration.hide() : numeration.show());



    // $.each(numerationSwitch,()=>{
    //     console.log($(this));
    // });
});
nameNumber.change(()=>{
    let chosenName = nameNumber.val();
    if(chosenName !=0){
        changable.show();
    }
    else{
        changable.hide();
    }
});
typeNumber.change(()=> {
    let typeId =  $('#select_type_number option:selected').val();
    nameNumber.prop('selectedIndex',0);
    if(typeId != 0 ){
        nameNumber.removeAttr('disabled');
    }
    else{
        nameNumber.attr('disabled',true);
        changable.hide();
    }
    $.each(nameNumber.children('option'),function(index){
        let option = $(this);
        if(option.data('value') != typeId && option.data('value') != 0){
            option.hide();
            option.attr('disabled',true);
        }
        else{
            option.attr('disabled',false);
            option.show();
        }
    })
});

let btnAddBads = $('#btnAddBads');
let btnRemoveBad = $('.btnRemoveBad');
let elementBadHtml = `<div class="input-block with-trash">
                    <div class="input half">
                        <div class="half-block max-992 mobile-mb-30">
                            <p>Тип кровати</p>
                            <div class="d-inline-block selectize-control">
                            <select class="form-control custom-select h-auto" name="bed[]">
                                @foreach($bed as $item)
                                    <option value="{{ $item->id  }}">{{ $item->name_ru  }}</option>
                                @endforeach
                            </select>
                            </div>
                            <span class="btnRemoveBad trash-icon"><i class="fa-lg fe fe-trash-2"></i></span>
                        </div>
                        <div class="half-block max-992">
                            <p>Количество</p>
                            <input required=required" type="number" name="bed_count[]" min="1" value="1">
                        </div>
                    </div>
                    <div class="text-block">
                        <a href="#" class="btnRemoveBad"><i class="fa-lg fe fe-trash-2"></i></a>
                    </div>
                    </div>`;
btnAddBads.on('click', function() {
  $(this).parent().before(elementBadHtml)
})
$('#badsBlock').on('click', '.btnRemoveBad', function(e) {
  e.preventDefault();
  $(this).closest('.with-trash').remove();
})

$('.btn_saveOrFinish').on('click', function(e) {
  e.preventDefault();
})
let checkboxes = $('.checkboxes');
checkboxes.each(function() {
        let action = $(this).find('.action');
        let changer = ($(this).find('.changer'));
        changer.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
        });
        action.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
        });
});
</script>
@endsection

