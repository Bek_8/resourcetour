@extends('layouts.backend')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
        <h3 class="card-title">Бронь номера </h3>
         <a class="btn btn-sm btn-outline-success" href="{{ action('CalendarController@create', $id) }}">Добавить</a>
        </div>
            <div id='calendar'></div>
        </div>
    </div>
</div>
@endsection
@section('style')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
@endsection
@section('script')
<script>
var __origDefine = define;
define = null;
</script>
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('backend/plugins/fullcalendar/js/moment.min.js') }}"></script>
<script src="{{ asset('backend/plugins/fullcalendar/js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('backend/plugins/fullcalendar/js/locales.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#calendar').fullCalendar({
          locales: 'ru',
          lang: 'ru',
            events : [
                @foreach($data as $datas)
                {
                    title : 'Усернейм и данные брони',
                    start : '2019-10-13',
                    end : '2019-10-16',
                    url : '2019-10-15'
                },
                @endforeach
            ]
        })
    });
</script>

@endsection
