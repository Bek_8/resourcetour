@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Изменить Номер</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('HotelController@update', $data->id) }}" method="POST" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name">Название</label>
                            <input value="{{ $data->name }}" required="required" name="name" type="text" class="form-control" id="name">
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="desc">Описание</label>--}}
{{--                            <input value="{{ $data->description }}" required="required" name="description" type="text" class="form-control" id="desc">--}}
{{--                        </div>--}}
                        <div class="form-group">
                            Категория
                            <select name="city_id" class="form-control">
                                @foreach( $city as $datas )
                                <option value="{{ $datas->id }}"
                                @if($datas->id == $data->city_id) selected @endif>
                                    {{ $datas->name_ru }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Опции</label>
                            <div class="selectgroup selectgroup-pills">
                              <label class="selectgroup-item">
                                <input type="checkbox" {{ $data->transfer ? 'checked' : null  }} name="transfer" value="1" class="selectgroup-input" />
                                <span class="selectgroup-button">Трансфер</span>
                              </label>
                              <label class="selectgroup-item">
                                <input type="checkbox" {{ $data->breakfast ? 'checked' : null  }} name="breakfast" value="1" class="selectgroup-input" />
                                <span class="selectgroup-button">Завтрак</span>
                              </label>
                              <label class="selectgroup-item">
                                <input type="checkbox" {{ $data->wifi ? 'checked' : null  }} name="wifi" value="1" class="selectgroup-input" />
                                <span class="selectgroup-button">Wifi</span>
                              </label>
                              <label class="selectgroup-item">
                                <input type="checkbox" {{ $data->animals ? 'checked' : null  }} name="animals" value="1" class="selectgroup-input" />
                                <span class="selectgroup-button">Животные</span>
                              </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Локация</label>
                            <div id="map" style="height: 400px; width: 500px;"></div>
                            <input required="required" id="long" value="{{$data->long}}" name="long" type="hidden" class="form-control">
                            <input required="required" id="lat" value="{{$data->lat}}" name="lat" type="hidden" class="form-control">
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <img src="{{ asset('uploads/products/'.$data->id.'.jpg') }}" width="140">--}}
{{--                            Для изменения загрузите новое фото <span class="font-weight-bold">840x520</span>--}}
{{--                            <input type="file" class="form-control" required="required" name="image">--}}
{{--                        </div>--}}
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script>
    var lattitude = $('#lat');
    var longitude = $('#long');
function initMap() {
var latlng = new google.maps.LatLng({{$data->lat}}, {{$data->long}});
var map = new google.maps.Map(document.getElementById('map'), {
    center: latlng,
    zoom: 12,
        animation:google.maps.Animation.BOUNCE
});
var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    draggable: true
});
var lat ,long;
google.maps.event.addListener(marker, 'dragend', function (event) {
    lat  = this.getPosition().lat().toFixed(6);
    long = this.getPosition().lng().toFixed(6);
    lattitude.val(lat);
    longitude.val(long);
});
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&callback=initMap"></script>
@endsection
