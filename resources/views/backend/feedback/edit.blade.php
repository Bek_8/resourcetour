@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Изменить</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('FeedbackController@update',$id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name">Загаловок</label>
                            <input value="{{ $data->name }}" name="name" type="text" class="form-control" id="name" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="description">Описание</label>
                            <input value="{{ $data->description }}" name="description" type="text" class="form-control" id="description" placeholder="Введите описание">
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
