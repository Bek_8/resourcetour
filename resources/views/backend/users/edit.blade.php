@extends('layouts.backend')

@section('content')
@include('partials.admin_header')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Пользователь</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('ManagerController@update', $data->id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Имя</label>
                            <input type="text" class="form-control" name="name" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <label for="food-desc-uz">Email</label>
                            <input type="text" class="form-control" name="email" value="{{ $data->email }}">
                        </div>
                        <div class="form-group">
                            <label for="phone">Номер</label>
                            <input name="phone" type="text" class="form-control" id="phone" value="{{ $data->phone }}" placeholder="Введите Номер" checked="checked" />
                        </div>
                        <div class="form-group">
                            <label>Задать Роль</label>
                            <select name="role" class="form-control">
                                @foreach( $role as $datas )
                                <option value="{{ $datas->type }}" @if( $datas->type == $data->roles[0]->type) selected @endif>
                                    @if( $datas->type == 'admin' ) Модератор @endif
                                    @if($datas->type == 'manager') Менеджер @endif
                                    @if($datas->type == 'callcenter') Оператор @endif
                                    @if($datas->type == 'member') Гость @endif
                                </option>
                                @endforeach
                            </select>
                        </div>
                        @if($data->roles[0]->type != 'member')
                        <div class="form-group">
                            <label for="food-desc-uz">Новый пароль</label>
                            <input type="text" class="form-control" name="password" >
                        </div>
                        @endif
                        <button class="btn btn-success">Изменить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
