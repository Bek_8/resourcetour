@extends('layouts.app')

@section('content')
<div class="container my-3 my-md-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Изменить</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('TypeController@update',$id) }}" method="POST" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name_ru">Название</label>
                            <input value="{{ $data->text_ru }}" name="text_ru" type="text" class="form-control" id="name_ru" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="name_en">Название</label>
                            <input value="{{ $data->text_en }}" name="text_en" type="text" class="form-control" id="name_en" placeholder="Введите название">
                        </div>
                        <div class="form-group pt-5">
                            <div class="d-flex align-items-center">
                                <img src="{{ asset('uploads/type/'.$id.'.jpg') }}" alt="" width="264" class="mb-5" />
                                <span class="pl-5">Загрузите для изменения</span>
                            </div>
                            <input type="file" class="form-control-file" name="image" />
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
