@extends('layouts.backend')

@section('content')
@include('partials.admin_header')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Тип обьекта</h3>
    <a class="btn btn-sm btn-outline-success" href="{{ action('TypeController@create') }}">Добавить</a>
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>Название</th>
            <th>Название (EN)</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
            <td>{{ $datas->text_ru }}</td>
            <td>{{ $datas->text_en }}</td>
            <td class="text-right">
                <a href="{{ action('RoomTypeController@index' , $datas->id) }}" class="btn btn-warning">
                    <i class="fe fe-arrow-right"></i>
                </a>
                <a href="{{ action('TypeController@edit' , $datas->id) }}" class="btn btn-primary">
                    <i class="fe fe-edit"></i>
                </a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
</div>
@endsection
