@extends('layouts.app')

@section('content')
<div class="container my-3 my-md-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Добавить Тип обьекта</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('TypeController@store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label for="name">Название(RU)</label>
                            <input required="required" name="text_ru" type="text" class="form-control" id="name" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="name-en">Название(EN)</label>
                            <input required="required" name="text_en" type="text" class="form-control" id="name-en" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="photo">Название(EN)</label>
                            <input required="required" name="image" type="file" class="form-control" id="photo">
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
