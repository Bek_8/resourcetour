@extends('layouts.backend')

@section('content')
@include('partials.header')
	<section>
		<div class="container">
			<div class="row">
				<form action="#" autocomplete="off">
                    <div class="white-block mb-30">
                        @if(count($data) == 0)
                        <div class="head">
                            <h3><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-briefcase"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg> Партнеры</h3>
                        </div>
                        @endif
                        <div class="content list">
                        <ul class="list-block partner">
                            @if(count($data) == 0)
                                <p class="empty-text">Вы сможете указывать их при добавлении броней и вести учет по поступившим броням от партнеров.</p>
                            @endif
                            @foreach($data as $partner)
                            <li class="item">
                                <div class="left-block">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-briefcase"><rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect><path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path></svg>
                                    <p>{{ $partner->name }}</p>
                                </div>
                                <div class="phone">
                                    +{{ preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $partner->phone) }}
                                </div>
                                <div class="email">
                                    <a href="mailto:{{ $partner->email }}">{{ $partner->email }}</a>
                                </div>
                                <div class="right-block long">
                                    <span class="icon">
                                        <label class="custom-switch">
                                          <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" @if($partner->enabled) checked="checked" @endif>
                                          <span class="custom-switch-indicator actioner" data-href="{{action('PartnerController@switch',$partner->id)}}"></span>
                                          <span class="custom-switch-description"></span>
                                        </label>
                                    </span>
                                    <div class="icons">
                                        <a href="{{ action('PartnerController@edit',$partner->id)  }}" class="edit icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></a>
                    {{--					<form class="d-inline-block" action="{{ action('PartnerController@delete' , $partner->id) }}" onclick="return confirm('Вы уверены?')" method="POST">--}}
                    {{--					@method('DELETE')--}}
                    {{--					@csrf--}}
                    {{--						<button type="submit" class="trash icon">--}}
                    {{--							<i class="fa-lg fe fe-trash-2"></i>--}}
                    {{--						</button>--}}
                    {{--					</form>--}}
                                    </div>
                                </div>
                                <div class="text-block">
                                    <p>{{ $partner->description  }}</p>
                                </div>
                            </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ action('PartnerController@create',$id) }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                        </div>
                    </div>
					<div class="button-block">
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection

@section('script')
<script>
    $('.actioner').on('click',function () {
        let url = $(this).data('href');
        $.get(url);
    })
</script>
@endsection
