@extends('layouts.backend')

@section('content')
@include('partials.admin_header')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Новости</h3>
    <a class="btn btn-sm btn-outline-success" href="{{ action('NewsController@create') }}">Добавить</a>
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>Название</th>
            <th>Описание</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
            <td>{{ $datas->name }}</td>
            <td>{{ $datas->description }}</td>
            <td class="text-right">
                <a href="{{ action('NewsController@edit' , $datas->id) }}" class="btn btn-primary">
                    <i class="fe fe-edit"></i>
                </a>
                <form class="d-inline-block" action="{{ action('NewsController@delete' , $datas->id) }}" method="POST">
                @method('DELETE')
                @csrf
                    <button class="btn btn-danger" onclick="return confirm('Вы уверены?')">
                        <i class="fe fe-trash"></i>
                    </button>
                </form>
            </td>
            <td>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
</div>
@endsection
