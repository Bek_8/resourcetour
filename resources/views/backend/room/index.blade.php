@extends('layouts.backend')

@section('content')
@include('partials.header')
	<section>
		<div class="container">
			<div class="row">
			<h2 class="blue-title">Управление номерами</h2>
				<form action="#" autocomplete="off">
					<div class="white-block">
						<ul class="list-block">
                            @foreach($data as $room)
							<li class="item">
								<div class="left-block">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>
									<p>{{ $room->room_name()->first()->name_ru }}</p>
								</div>
								<div class="right-block">
									<span class="icon">
										<label class="custom-switch">
										  <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" @if($room->status) checked="checked" @endif>
										  <span class="custom-switch-indicator actioner" data-href="{{action('RoomController@switch',$room->id)}}"></span>
										  <span class="custom-switch-description"></span>
										</label>
									</span>

									<a href="{{ action('RoomController@edit',$room->id)  }}" class="edit icon">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                        </svg>
                                    </a>
                                    <a href="{{ action('RoomController@delete' , $room->id) }}" onclick="return confirm('Вы уверены?')" class="trash icon">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2">
                                            <polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                    </a>
								</div>
							</li>
                            @endforeach
							<a class="add-list-btn" href="{{ action('RoomController@create',$id) }}">
								<i class="fe fe-plus-circle"></i>
								Добавить номер
							</a>
						</ul>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection

@section('script')
<script>
    $('.actioner').on('click',function () {
        let url = $(this).data('href');
        $.get(url);
    })
</script>
@endsection
