@extends('layouts.frontend')

@section('content')
@include('front_partials.nav-light')
<div class="nav-light">
    <div class="container">
        <div class="row d-flex">
            <div class="col-4">
                <div class="nav-light__links d-flex align-items-center justify-content-lg-start justify-content-center">
                    <i data-feather="home"></i>
                    <span class="md-hide">Object choice</span>
                </div>
            </div>
            <div class="col-4">
                <div class="nav-light__links d-flex align-items-center justify-content-center">
                    <i data-feather="lock"></i>
                    <span class="md-hide">Reservation</span>
                </div>
            </div>
            <div class="col-4">
                <div class="nav-light__links link-active d-flex align-items-center justify-content-lg-end justify-content-center">
                    <i data-feather="check-circle"></i>
                    <span class="md-hide">Confirmation</span>
                </div>
            </div>
        </div>
    </div>
</div>
<main role="main" class="confirmation">
    <div class="container">
        <div class="row">
            <div class="confirmation__main d-flex align-items-lg-center align-items-start justify-content-between w-100 flex-wrap">
                <div class="col-lg-6 h-100 left d-flex flex-column align-items-lg-start align-items-center justify-content-center">
                    <img src="{{ asset('images/success.svg')  }}" alt="success" />
                    <div class="title">Your reservation at Silk Road Bukhara Hotel is confirmed</div>
                </div>
                <div class="col-lg-6 h-100 right d-flex flex-column justify-content-center align-items-lg-start align-items-center">
                    <div class="title">Reservations is always available in your account</div>
                    <div class="description">You can check your reservation in your account. We sent the login information to your e-mail.</div>
                    <a href="{{action('PageController@bookings')}}" class="button text-center">My reservation</a>
                    <div class="print d-flex">
                        <div class="icon d-flex align-items-center"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M6 9V2H18V9" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M6 18H4C3.46957 18 2.96086 17.7893 2.58579 17.4142C2.21071 17.0391 2 16.5304 2 16V11C2 10.4696 2.21071 9.96086 2.58579 9.58579C2.96086 9.21071 3.46957 9 4 9H20C20.5304 9 21.0391 9.21071 21.4142 9.58579C21.7893 9.96086 22 10.4696 22 11V16C22 16.5304 21.7893 17.0391 21.4142 17.4142C21.0391 17.7893 20.5304 18 20 18H18" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M18 14H6V22H18V14Z" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg></div>
                        <div class="print-title">Confirmation</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('script')
@endsection
