@extends('layouts.frontend')

@section('content')
@include('front_partials.nav-light')
@include('front_partials.nav-primary')
    <div class="change">
        <div class="container">
            <div class="row">
                <div class="col-12 py-0">
                    <div class="change__top d-flex flex-wrap flex-lg-row flex-column align-items-center">
                        <div class="col-lg-5 py-0 left d-flex align-items-center">
                            <img src="{{ $booking->room->image[0] }}" alt="photo" />
                            <div class="titles">
                                <div class="name">{{$booking->room->room_type->name_en}} {{$booking->room->room_name->name_en}}</div>
                                <div class="hotel">{{$booking->name}}</div>
                            </div>
                        </div>
                        <div class="col-lg-5 py-0 center">
                            <div class="date">{{\Carbon\Carbon::parse($booking->from)->isoFormat('MMMM D ,Y')}} — {{\Carbon\Carbon::parse($booking->to)->isoFormat('MMMM D ,Y')}}</div>
                            <div class="days">{{$nights}} days</div>
                        </div>
                        <div class="col-lg-2 py-0 right">
                            <div class="price">USD {{$booking->hotel->getBookingInformation()['price']}}</div>
                            <div class="persons">{{$booking->hotel->getBookingInformation()['adults']}} persons</div>
                        </div>
                        <input type="text" class="daterange" id="daterange" name="period" style="position: absolute;bottom: -5px;right: 0;left: 0; visibility: hidden;" />
                    </div>
                    <div class="change__date"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    $(function () {
        let minDate = new Date('{{\Carbon\Carbon::parse($booking->from)}}');
        let maxDate = new Date('{{\Carbon\Carbon::parse($booking->to)}}');
        let nights = {{$nights}};
          if ($(window).width() < 991) {
              $('#daterange').daterangepicker({
                    autoUpdateInput: true,
                    constrainInput: false,
                    singleDatePicker: false,
                    linkedCalendars: false,
                    parentEl: $('.change__date'),
                    maxSpan: {"days": nights},
                    alwaysShowCalendars: true,
                    autoApply: true,
                    minDate: minDate,
                    startDate: minDate,
                    maxDate: maxDate,
                    locale: {
                        "format": "MMMM D",
                        "separator": " - ",
                        "firstDay": 1,
                        applyLabel: 'Show prices'
                    },
                }, function(start, end, label) {
                   let params = {
                        from: moment(start).format('D-M-Y'),
                        to: moment(end).format('D-M-Y')
                    };
                });
            }
            else {
              $('#daterange').daterangepicker({
                  autoUpdateInput: true,
                  constrainInput: true,
                  singleDatePicker: false,
                  autoApply: true,
                  minDate: minDate,
                  startDate: minDate,
                  maxDate: maxDate,
                  maxSpan: {"days": nights,
                  opens: 'center',
                  locale: {
                      "format": "MMMM D",
                      "separator": " - ",
                      "fromLabel": "From",
                      "toLabel": "To",
                      "customRangeLabel": "Custom",
                      "weekLabel": "W",
                      "daysOfWeek": [
                          "Su",
                          "Mo",
                          "Tu",
                          "We",
                          "Th",
                          "Fr",
                          "Sa"
                      ],
                      "monthNames": [
                          "January",
                          "February",
                          "March",
                          "April",
                          "May",
                          "June",
                          "July",
                          "August",
                          "September",
                          "October",
                          "November",
                          "December"
                      ],
                      "firstDay": 1
                  },
              }, function (start, end, label) {
                  let params = {
                      from: moment(start).format('D-M-Y'),
                      to: moment(end).format('D-M-Y')
                  };
                    {{--let esc = encodeURIComponent;--}}
                    {{--let query = Object.keys(params)--}}
                    {{--.map(k => esc(k) + '=' + esc(params[k]))--}}
                    {{--.join('&');--}}
                    {{--let url = "{{ action('BookingController@get',$booking->id) }}?" + query;--}}
                    {{--window.location.href = url;--}}
                  }
              });
          }
        let picker = $('#daterange');
        picker.data('daterangepicker').hide = function () {};
        picker.data('daterangepicker').show();
        $('#daterange').on('apply.daterangepicker', function(ev, picker) {
            let params = {
              from: moment(picker.startDate).format('D-M-Y'),
              to: moment(picker.endDate).format('D-M-Y')
            };
            let esc = encodeURIComponent;
            let query = Object.keys(params)
            .map(k => esc(k) + '=' + esc(params[k]))
            .join('&');
            let url = "{{ action('PageController@newDates',$booking->id) }}?" + query;
            window.location.href = url;
        });
    });
</script>
@endsection
