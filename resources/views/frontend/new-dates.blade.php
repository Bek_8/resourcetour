@extends('layouts.frontend')

@section('content')
@include('front_partials.nav-light')
@include('front_partials.nav-primary')
<div class="change">
    <div class="container">
        <div class="row">
            <div class="col-12 py-0">
                <form action="{{action('PageController@changeBook',$booking->id)}}" method="POST" class="dates__item">
                    @csrf
                    <div class="confirm"><div class="top">Confirm new dates</div></div>
                    <div class="change__top no-shadow d-flex flex-wrap flex-lg-row flex-column align-items-center">
                        <div class="col-lg-5 py-0 left d-flex align-items-center">
                            <img src="{{ $booking->room->image[0] }}" alt="photo" />
                            <div class="titles">
                                <div class="name">{{$booking->room->room_type->name_en}} {{$booking->room->room_name->name_en}}</div>
                                <div class="hotel">{{$booking->name}}</div>
                            </div>
                        </div>
                        <div class="col-lg-5 py-0 center">
                            <div class="date">{{\Carbon\Carbon::parse($from)->isoFormat('MMMM D ,Y')}} — {{\Carbon\Carbon::parse($to)->isoFormat('MMMM D ,Y')}}</div>
                            <div class="days">{{$nights}} days</div>
                        </div>
                        <div class="col-lg-2 py-0 right">
                            <div class="price">USD {{ $price }}</div>
                            <div class="persons">{{$booking->hotel->getBookingInformation()['adults']}} persons</div>
                        </div>
                        <input type="hidden" value="{{$from .' , '.$to}}" name="dates" />
                    </div>
                    <div class="buttons d-flex flex-lg-wrap flex-lg-row flex-column align-items-center justify-content-center">
                        <button class="dates__confirm">Confirm</button>
                        <a href="{{action('PageController@bookings')}}" class="dates__cancel">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
