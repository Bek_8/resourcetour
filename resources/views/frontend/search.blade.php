@extends('layouts.frontend')

@section('content')
@include('front_partials.nav')
<main role="main">
    <search></search>
</main>
@endsection

@section('script')
<script>
    let search;
    Vue.component('search', {
        data() {
            return {
                hotels: @JSON($hotels),
                hotel_types: @JSON($type),
                services: @JSON($services),
                roomServices: @JSON($room_services),
                chosenType: 1,
                filterType: 1,
                roomFilterCheckbox: [],
                hotelFilterCheckbox: [],
                applySort: false
            }
        },
        mounted() {
            search = this;
        },
        methods: {
            setType: function (id) {
                this.chosenType = id;
            },
            setFilter: function(id){
                this.filterType = id;
            },
            getSorted: function () {
                let hotelService = this.hotelFilterCheckbox;
                let roomService = this.roomFilterCheckbox;
                return this.hotels.filter(hotel => {
                    let isServiceExist = true;
                    if (hotelService.length > 0) {
                        let sortByHotel = Object.keys(JSON.parse(hotel.services)).map(key => parseInt(key));
                        isServiceExist = sortByHotel.filter(function (service) {
                            return hotelService.indexOf(service) !== -1;
                        }).length > 0;
                    }
                    if ((roomService.length > 0) && isServiceExist) {
                        hotel.rooms.map(room => {
                            let sortByRoom = Object.keys(JSON.parse(room.services)).map(key => parseInt(key));
                            isServiceExist = sortByRoom.filter(function (service) {
                                return roomService.indexOf(service) !== -1;
                            }).length > 0;
                        });
                    }
                    if(isServiceExist) return hotel;
                });
            },
            closeModal : function () {
                $('.modal').removeClass('show');
                $('body').removeClass('no-scroll');
            },
            applyFilter : function () {
                this.applySort = true;
                this.closeModal();
            },
            removeFilters: function () {
                this.roomFilterCheckbox = [];
                this.hotelFilterCheckbox = [];
                this.applySort = false;
            }
        },
        computed: {
            getFiltered: function(){
                let hotelService = this.hotelFilterCheckbox.length;
                let roomService = this.roomFilterCheckbox.length;
                if((roomService || hotelService) && this.applySort) return this.getSorted();
                return this.hotels;
            },
            getSortedCount: function () {
                return this.getSorted().length;
            }
        },
        template: `
    <div class="search-wrap">
        <header class="info">
            <div class="container">
                <div class="row d-flex justify-content-between align-items-center flex-wrap">
                <div class="col-8">
                    <h2>{{$keyword ?? 'No keyword'}}: {{$hotels->count()}} properties found </h2>
                </div>
                <div class="col-3">
                    <div class="d-flex align-items-center justify-content-end">
                        <button class="filter__button">
                            <img src="{{asset('/images/filter.svg')}}" alt="filter">
                            <span class="text">
                                Show filters
                            </span>
                        </button>
                    </div>
                </div>
                </div>
            </div>
        </header>
        <div class="menu filter-toggler">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 d-flex flex-lg-row flex-column  justify-content-between align-items-center flex-wrap">
                        <div v-on:click="setFilter(1)" v-bind:class="'menu-search ' + (filterType === 1 ? 'active' : '')">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.83332 18.3333H3.33332C2.8913 18.3333 2.46737 18.1577 2.15481 17.8452C1.84225 17.5326 1.66666 17.1087 1.66666 16.6667V10.8333C1.66666 10.3913 1.84225 9.96737 2.15481 9.65481C2.46737 9.34225 2.8913 9.16666 3.33332 9.16666H5.83332M11.6667 7.49999V4.16666C11.6667 3.50362 11.4033 2.86773 10.9344 2.39889C10.4656 1.93005 9.8297 1.66666 9.16666 1.66666L5.83332 9.16666V18.3333H15.2333C15.6353 18.3379 16.0253 18.197 16.3316 17.9367C16.6379 17.6763 16.8397 17.3141 16.9 16.9167L18.05 9.41666C18.0862 9.17779 18.0701 8.93389 18.0028 8.70187C17.9354 8.46984 17.8184 8.25524 17.6599 8.07292C17.5013 7.8906 17.3051 7.74493 17.0846 7.646C16.8642 7.54708 16.6249 7.49725 16.3833 7.49999H11.6667Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            <span class="text">Recommended</span>
                        </div>
                        <div v-on:click="setFilter(2)" v-bind:class="'menu-search ' + (filterType === 2 ? 'active' : '')">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10 1.66666L12.575 6.88332L18.3334 7.72499L14.1667 11.7833L15.15 17.5167L10 14.8083L4.85002 17.5167L5.83335 11.7833L1.66669 7.72499L7.42502 6.88332L10 1.66666Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            <span class="text">Rating</span>
                        </div>
                        <div v-on:click="setFilter(3)" v-bind:class="'menu-search ' + (filterType === 3 ? 'active' : '')">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10 4.16666V15.8333" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M15.8334 10L10 15.8333L4.16669 10" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            <span class="text">Price — down</span>
                        </div>
                        <div v-on:click="setFilter(4)" v-bind:class="'menu-search ' + (filterType === 4 ? 'active' : '')">

                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.8333 4.16666L4.16663 15.8333" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M5.41671 7.50001C6.5673 7.50001 7.50004 6.56727 7.50004 5.41668C7.50004 4.26608 6.5673 3.33334 5.41671 3.33334C4.26611 3.33334 3.33337 4.26608 3.33337 5.41668C3.33337 6.56727 4.26611 7.50001 5.41671 7.50001Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M14.5833 16.6667C15.7339 16.6667 16.6667 15.7339 16.6667 14.5833C16.6667 13.4327 15.7339 12.5 14.5833 12.5C13.4327 12.5 12.5 13.4327 12.5 14.5833C12.5 15.7339 13.4327 16.6667 14.5833 16.6667Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            <span class="text">Special offers</span>
                        </div>
                        <div v-on:click="setFilter(5)" v-bind:class="'menu-search ' + (filterType === 5 ? 'active' : '')">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M17.5 8.33334C17.5 14.1667 10 19.1667 10 19.1667C10 19.1667 2.5 14.1667 2.5 8.33334C2.5 6.34422 3.29018 4.43656 4.6967 3.03004C6.10322 1.62352 8.01088 0.833344 10 0.833344C11.9891 0.833344 13.8968 1.62352 15.3033 3.03004C16.7098 4.43656 17.5 6.34422 17.5 8.33334Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M10 10.8333C11.3807 10.8333 12.5 9.71405 12.5 8.33334C12.5 6.95263 11.3807 5.83334 10 5.83334C8.61929 5.83334 7.5 6.95263 7.5 8.33334C7.5 9.71405 8.61929 10.8333 10 10.8333Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            <span class="text">Great location</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="other filter-padding">
            <div class="container">
                <div class="row">
                    <div class="other__block d-flex align-items-center justify-content-between flex-wrap w-100">
                        <a v-bind:href="'/hotel/'+hotel.id" class="item" v-for="(hotel , key) in getFiltered" key="key">
                            <div class="top"></div>
                            <div class="bottom">
                                <div class="left">
                                    <div class="title">@{{hotel.name }}</div>
                                    <div class="location d-flex">
                                        <div class="icon d-flex align-items-center justify-content-center">
                                            <svg width="18" height="18"
                                                 viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16 7.36364C16 13.0909 8.5 18 8.5 18C8.5 18 1 13.0909 1 7.36364C1 5.41068 1.79018 3.53771 3.1967 2.15676C4.60322 0.775809 6.51088 0 8.5 0C10.4891 0 12.3968 0.775809 13.8033 2.15676C15.2098 3.53771 16 5.41068 16 7.36364Z" fill="#43F4EE"/>
                                            <path d="M8.5 11C10.433 11 12 9.433 12 7.5C12 5.567 10.433 4 8.5 4C6.567 4 5 5.567 5 7.5C5 9.433 6.567 11 8.5 11Z" fill="#00847F"/>
                                            </svg>
                                        </div>
                                        @{{hotel.city.name_en}}, @{{hotel.city.country.name_en}}
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="stars d-flex justify-content-end">
                                        <div class="icon">
                                            <svg width="32" height="32"
                                                 viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect width="32" height="32" rx="16" fill="white"/>
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16 10C15.7774 10 15.5925 10.1564 15.5349 10.3704L14.3549 14.1739H10.4799C10.2124 14.1739 10 14.4167 10 14.6982C10 14.8705 10.0825 15.0243 10.2075 15.1158C10.2849 15.1705 13.3374 17.4974 13.3374 17.4974C13.3374 17.4974 12.1651 21.2541 12.1423 21.314C12.125 21.3687 12.1125 21.4288 12.1125 21.4913C12.1125 21.773 12.3298 22 12.5974 22C12.7 22 12.7949 21.9661 12.875 21.9088L16 19.5347C16 19.5347 19.055 21.8566 19.125 21.9088C19.2048 21.9661 19.3001 22 19.4023 22C19.6699 22 19.8875 21.7705 19.8875 21.4913C19.8875 21.4288 19.8751 21.3687 19.8574 21.314C19.835 21.2541 18.6626 17.4974 18.6626 17.4974C18.6626 17.4974 21.7149 15.1705 21.7925 15.1158C21.9175 15.0243 22 14.8705 22 14.6957C22 14.4167 21.7925 14.1739 21.5249 14.1739H17.6499L16.4648 10.3704C16.4075 10.1564 16.2224 10 16 10Z" fill="currentColor"/>
                                            </svg>
                                            <div class="rating">9/10</div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        Start from <span class="golden">USD 175</span>
                                    </div>
                                </div>
                            </div>
                            </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal">
            <div class="modal-bg"></div>
                <div class="filter">
                    <div class="close close-modal" v-on:click="closeModal">
                        <svg width="24" height="24" viewBox="0 0 24 24"
                             fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18 6L6 18" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M6 6L18 18" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                    <button class="filter__label d-flex align-items-center">
                        <svg width="24" height="24"
                                 viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M8 12H16" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        <span class="text">Type of housing</span>
                    </button>
                    <div class="filter__choice d-flex justify-content-between">
                        <div class="item" v-for="(type,key) in hotel_types" key="key">
                            <img :src="'/uploads/type/'+type.id+'.jpg'" alt="type" />
                            <input v-bind:id="'type'+type.id" type="radio" v-on:change="setType(type.id)" v-bind:value="type.id" :checked="key === 0" />
                            <label v-bind:for="'type'+type.id"><span>@{{ type.text_ru }}</span></label>
                        </div>
                    </div>
                    <div class="filter__choice">
                    <button class="filter__label d-flex align-items-center">
                        <svg width="24" height="24"
                                 viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M8 12H16" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        <span class="text">Room services and facilities</span>
                    </button>
                        <div class="d-flex flex-wrap justify-content-between">
                            <div class="filter__block col-6" v-for="(service, key) in services" v-bind:key="'service'+key">
                                <div class="title">@{{service.name_ru}}</div>
                                <div class="checkboxes" v-for="(inner , key) in service.inners" v-bind:key="'inner'+key">
                                    <input type="checkbox" v-model="hotelFilterCheckbox" v-bind:id="'check'+inner.id" v-bind:value="inner.id">
                                    <label v-bind:for="'check'+inner.id">@{{inner.name_ru}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filter__bottom">
                    <button class="filter__label d-flex align-items-center">
                        <svg width="24" height="24"
                                 viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M8 12H16" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        <span class="text">Object services and facilities</span>
                    </button>
                        <div class="d-flex flex-wrap justify-content-between">
                            <div class="filter__block col-6" v-for="(roomService, key) in roomServices" v-bind:key="'roomService'+key">
                                <div class="title">@{{roomService.name_ru}}</div>
                                <div class="checkboxes" v-for="(inner , key) in roomService.inners" v-bind:key="'roomInner'+key">
                                    <input type="checkbox" v-model="roomFilterCheckbox" v-bind:id="'roomCheck'+inner.id" v-bind:value="inner.id">
                                    <label v-bind:for="'roomCheck'+inner.id">@{{inner.name_ru}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filter__apply d-flex align-items-center">
                        <button class="button" v-on:click="applyFilter"> Show @{{ getSortedCount }} results </button>
                        <button class="clear d-flex align-items-center" v-on:click="removeFilters">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M2.5 5H4.16667H17.5" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M6.66699 4.99996V3.33329C6.66699 2.89127 6.84259 2.46734 7.15515 2.15478C7.46771 1.84222 7.89163 1.66663 8.33366 1.66663H11.667C12.109 1.66663 12.5329 1.84222 12.8455 2.15478C13.1581 2.46734 13.3337 2.89127 13.3337 3.33329V4.99996M15.8337 4.99996V16.6666C15.8337 17.1087 15.6581 17.5326 15.3455 17.8451C15.0329 18.1577 14.609 18.3333 14.167 18.3333H5.83366C5.39163 18.3333 4.96771 18.1577 4.65515 17.8451C4.34259 17.5326 4.16699 17.1087 4.16699 16.6666V4.99996H15.8337Z" stroke="#00236D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            <span class="text">Clear all</span>
                        </button>
                    </div>
            </div>
        </div>
    </div>
        `,
    });
    $(document).ready(function(){
        let today = new Date();
        let minDate = new Date('{{$from}}');
        let maxDate = new Date('{{$to}}');
          if ($(window).width() < 991) {
              $('#daterange_nav').daterangepicker({
                    autoUpdateInput: true,
                    constrainInput:false,
                    singleDatePicker: false,
                    linkedCalendars: false,
                    minDate: today,
                    applyButtonClasses: 'add-list-btn',
                    autoApply: true,
                    startDate: minDate,
                    endDate: maxDate,
                    locale: {
                        "format": "MMMM D",
                        "separator": " — ",
                        "firstDay": 1,
                    },
                }, function(start, end, label) {

                });
            }
            else {
              $('#daterange_nav').daterangepicker({
                  opens: 'center',
                  startDate: minDate,
                  endDate: maxDate,
                  autoUpdateInput: true,
                  constrainInput: true,
                  singleDatePicker: false,
                  minDate: today,
                  maxSpan: {
                      "days": 30
                  },
                  locale: {
                      "format": "MMMM D",
                      "separator": " — ",
                      "fromLabel": "From",
                      "toLabel": "To",
                      "customRangeLabel": "Custom",
                      "weekLabel": "W",
                      "daysOfWeek": [
                          "Su",
                          "Mo",
                          "Tu",
                          "We",
                          "Th",
                          "Fr",
                          "Sa"
                      ],
                      "monthNames": [
                          "January",
                          "February",
                          "March",
                          "April",
                          "May",
                          "June",
                          "July",
                          "August",
                          "September",
                          "October",
                          "November",
                          "December"
                      ],
                      "firstDay": 1
                  },
                    autoApply: true,
              }, function (start, end, label) {

              });
          }
        $('.filter-toggler').click(function(){
            $(this).toggleClass('open')
        });
        $('.filter__button').on('click',function(){
            $('.modal').addClass('show');
            $('body').addClass('no-scroll');
        });
        $('.modal-bg').on('click',function(){
            $('.modal').removeClass('show');
            $('body').removeClass('no-scroll');
        });
    });
</script>
@endsection
