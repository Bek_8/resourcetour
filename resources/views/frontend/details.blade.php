@extends('layouts.frontend')

@section('content')
@include('front_partials.nav-light')
@include('front_partials.nav-primary')
<div class="details-top">
    <div class="container">
        <div class="row">
            <div class="col-12 py-0 d-flex flex-wrap align-items-lg-center align-items-start justify-content-lg-between justify-content-center">
                <div class="col-lg-3 col-6 py-0">
                    <div class="name d-flex align-items-center">{{ $hotel->name }}</div>
                </div>
                <div class="divider d-flex flex-lg-row flex-column align-items-lg-center align-items-start col-lg-6 col-6 py-0">
                    <div class="box d-flex align-items-center justify-content-center">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <svg width="20" height="20"
                                 viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.5318 8.33332C9.85681 6.39164 8.04455 5 5.90909 5C3.19682 5 1 7.2375 1 10C1 12.7625 3.19682 15 5.90909 15C8.04455 15 9.85681 13.6083 10.5318 11.6667H14.0909V15H17.3636V11.6667H19V8.33332H10.5318ZM5.90909 11.6666C5.00502 11.6666 4.27272 10.9208 4.27272 9.99996C4.27272 9.0791 5.00502 8.33332 5.90909 8.33332C6.81317 8.33332 7.54547 9.07914 7.54547 10C7.54547 10.9209 6.81317 11.6666 5.90909 11.6666Z" fill="currentColor"/>
                            </svg>
                        </div>
                        <div class="inner-text">{{$hotel->getBookingInformation()['rooms'] ?? 2}} rooms</div>
                    </div>
                    <div class="box d-flex align-items-center justify-content-center">
                        <div class="icon d-flex align-items-center justify-content-center"><svg width="20" height="20" viewBox="0 0 20 20"
         fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M11.8174 12.3931C11.7636 11.7977 11.7843 11.3822 11.7843 10.8381C12.0532 10.6966 12.5351 9.79458 12.6165 9.03255C12.828 9.01519 13.1614 8.80834 13.2591 7.99165C13.3117 7.5532 13.1025 7.30646 12.975 7.22889C13.3191 6.19131 14.0338 2.98143 11.6531 2.64973C11.4081 2.2183 10.7807 2 9.96537 2C6.70337 2.06021 6.30989 4.46965 7.025 7.22889C6.89789 7.30646 6.68863 7.5532 6.74095 7.99165C6.83895 8.80834 7.172 9.01519 7.38347 9.03255C7.46453 9.79421 7.96558 10.6966 8.23526 10.8381C8.23526 11.3822 8.25553 11.7977 8.20174 12.3931C7.55626 14.1329 3.20116 13.6446 3 17H17C16.7992 13.6446 12.4629 14.1329 11.8174 12.3931Z" fill="currentColor"/>
    </svg></div>
                        <div class="inner-text">{{$hotel->getBookingInformation()['adults']}} persons</div>
                    </div>
                    <div class="box d-flex align-items-center justify-content-center">
                    <div class="icon d-flex align-items-center justify-content-center"><svg width="20" height="20"
                                                                                            viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<g clip-path="url(#clip0)">
<path d="M10 0.833328V19.1667" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M14.1667 4.16667H7.91667C7.14312 4.16667 6.40125 4.47396 5.85427 5.02094C5.30729 5.56793 5 6.30979 5 7.08334C5 7.85689 5.30729 8.59875 5.85427 9.14573C6.40125 9.69271 7.14312 10 7.91667 10H12.0833C12.8569 10 13.5987 10.3073 14.1457 10.8543C14.6927 11.4013 15 12.1431 15 12.9167C15 13.6902 14.6927 14.4321 14.1457 14.9791C13.5987 15.526 12.8569 15.8333 12.0833 15.8333H5" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
<defs>
<clipPath id="clip0">
<rect width="20" height="20" fill="white"/>
</clipPath>
</defs>
</svg></div>
                    <div class="inner-text">USD {{$hotel->getBookingInformation()['price']}}</div>
                </div>
                </div>
                <button class="button">Confirmation</button>
            </div>
        </div>
    </div>
</div>
@include('front_partials.message')
<div class="bookings">
    <div class="container">
        <div class="row">
            @foreach($bookings as $booking)
            <div class="bookings__item d-flex flex-wrap align-items-center w-100">
                <div class="left"><img src="{{$booking->hotel->image[0]}}" alt="photo"></div>
                <div class="right d-flex flex-column justify-content-between">
                    <div class="top d-flex align-items-center justify-content-between">
                        <div class="city">{{$booking->name}}</div>
                        <div class="persons">{{$booking->adult}} persons</div>
                    </div>
                    <div class="middle d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-center">
                        <div class="title">{{$booking->room->room_type->name_ru}} {{$booking->room->room_name->name_ru}}</div>
                        <div class="rooms">{{\Carbon\Carbon::parse($booking->from)->isoFormat('MMMM D,YYYY') .' — '.\Carbon\Carbon::parse($booking->to)->isoFormat('MMMM D,YYYY')}}</div>
                    </div>
                    <div class="bottom d-flex align-items-lg-center align-items-end justify-content-between">
                        <div class="links d-lg-block d-flex flex-column">
                            <a href="{{ action('PageController@change',$booking->id) }}">Change dates</a>
                            <a href="{{action('PageController@cancel',$booking->id)}}">Cancel</a>
                        </div>
                        <div class="total d-lg-block d-flex flex-column">USD {{number_format((float)$booking->price, 2, '.', '')}}</div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('script')
<script></script>
@endsection
