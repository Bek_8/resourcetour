@extends('layouts.frontend')

@section('content')
@include('front_partials.nav-light')
@include('front_partials.nav-primary')
<main role="main" class="confirmation">
    <div class="container">
        <div class="row">
            <div class="confirmation__main confirmation__main--mobile d-flex flex-column justify-content-center w-100">
                <div class="left pl-0 text-center">
                    <div class="title no-max">You have no reservations yet</div>
                </div>
                <div class="right right-empty text-center">
                    <div class="description">We have many great offers for you. Let's get a look?</div>
                    <button class="button">View offers</button>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('script')
@endsection
