@if($errors->any())
<div class="container mt-2">
    <div class="row">
    @if($errors->any())
        <div class="alert w-100 mb-0 alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif

    @if(session('success'))
        <div class="alert w-100 mb-0 alert-success">{{session('success')}}</div>
    @endif

    @if(session('error'))
        <div class="alert w-100 mb-0 alert-danger">{{session('error')}}</div>
    @endif
    </div>
</div>
@endif
