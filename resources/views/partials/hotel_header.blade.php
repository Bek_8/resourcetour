<header class="second">
    <div class="top-block">
        <div class="top-line">
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
            <div class="item blue"></div>
            <div class="item red"></div>
            <div class="item yellow"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="left-block">
                    <a href="{{ action('HomeController@index')  }}">
                        <img src="{{ asset('backend/images/logo.svg') }}" />
                    </a>
                    <span class="description">Hotel Management System</span>
                </div>
                <div class="right-block">
                    <a href="#" class="help">Помощь</a>
                    @include('partials.dropdown')
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-block menu">
        <div class="container">
            <div class="row">
                <div class="dropdown">
                    <a href="#" class="name" data-toggle="dropdown">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                        <span>{{ ( !isset($id)  ?  'Управление обьектами' : $hotels->where('id',$id)->first()->name) }}</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
@include('partials.message')
