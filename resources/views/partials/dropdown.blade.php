<div class="dropdown">
    <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
    <div class="user">
        <span class="circle">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
        </span>
        <span class="name">{{ Auth::user()->name }}</span>
    </div>
    </a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow dropdown-block">
    <button class="dropdown-item" type="submit"><a href="{{action('UserController@setting')}}"><i class="dropdown-icon fe fe-settings"></i> Настройки</a></button>
    <form method="POST" action="{{ route('logout') }}">
        @csrf
            <button class="dropdown-item" type="submit"><i class="dropdown-icon fe fe-log-out"></i> Выйти</button>
    </form>
    </div>
</div>
