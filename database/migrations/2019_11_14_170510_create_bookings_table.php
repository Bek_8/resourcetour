<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('hotel_id');
            $table->unsignedInteger('room_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('price');
            $table->unsignedInteger('room_count')->nullable();
            $table->string('name')->nullable();
            $table->string('country')->nullable();
            $table->boolean('paid')->default(0);
            $table->boolean('tourist_resource')->default(0);
            $table->string('phone');
            $table->string('email');
            $table->unsignedInteger('adult');
            $table->unsignedInteger('child');
            $table->unsignedInteger('numeration');
            $table->unsignedInteger('partner_id')->nullable();
            $table->unsignedInteger('status')->default(0);
            $table->date('from');
            $table->date('to');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
