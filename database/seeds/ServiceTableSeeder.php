<?php

use Illuminate\Database\Seeder;
use App\Service;
use App\RoomService;
class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::create([
            'name_ru'=>'Общее',
            'name_en'=>'All',
        ]);
        RoomService::create([
            'name_ru'=>'Общее',
            'name_en'=>'All',
        ]);
    }
}
