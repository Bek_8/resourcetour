<?php

use Illuminate\Database\Seeder;
use App\Bed;
class BedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bed::create([
            'name_ru'=>'Двуспальная кровать',
            'name_en'=>'Двуспальная кровать',
        ]);
        Bed::create([
            'name_ru'=>'Односпальная кровать',
            'name_en'=>'Односпальная кровать',
        ]);
    }
}
