<?php

use Illuminate\Database\Seeder;
use App\Hotel;

class HotelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cards = ['mastercard','visa','unionpay','jcb'];
        Hotel::create([
              "name" => "Hyatt Regency",
              "city_id" => "1",
              "user_id" => 1,
              "type_id" => "1",
              "address" => "Hyatt Regency",
              "phone" => "+990000000",
              "income" => "06:00",
              "income_to" => "06:00",
              "outcome" => "10:30",
              "outcome_to" => "10:30",
              "confirmation" => "1",
              "credit" => "1",
              "cards" => json_encode($cards,true),
              "animals" => "0",
              "services" => "{'1':2}",
              "status" => 0,
              "lat" => "41.311081",
              "long" => "69.240562",
        ]);
    }
}
