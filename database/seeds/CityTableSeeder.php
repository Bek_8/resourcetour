<?php

use Illuminate\Database\Seeder;
use App\City;
class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::create([
            'name_ru'=>'Ташкент',
            'name_en'=>'Tashkent',
            'country_id'=> 1,
        ]);
    }
}
