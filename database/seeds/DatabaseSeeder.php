<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RoleTableSeeder::class);
         $this->call(UserTableSeeder::class);
         $this->call(CountryTableSeeder::class);
         $this->call(CityTableSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(ServiceTableSeeder::class);
         $this->call(ServiceInnerTableSeeder::class);
         $this->call(TypeTableSeeder::class);
         $this->call(BedTableSeeder::class);
         $this->call(RoomTypeTableSeeder::class);
         $this->call(RoomNameTableSeeder::class);
         $this->call(HotelTableSeeder::class);
    }
}
