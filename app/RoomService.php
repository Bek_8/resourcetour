<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomService extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inners()
    {
        return $this->hasMany('App\RoomServiceInner');
    }

}
