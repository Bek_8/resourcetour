<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Hotel;

class HotelComposer {
    public function compose(View $view)
    {
        if(auth()->check()) {
            if (auth()->user()->hasRole('admin')) {
                $hotels = Hotel::all();
            } else {
                $hotels = Hotel::where('user_id', auth()->user()->id)->get();
            }
            view()->share('hotels', $hotels);
        }
        $avatar = auth()->user();
        $view->with('avatar', $avatar);
    }
}
