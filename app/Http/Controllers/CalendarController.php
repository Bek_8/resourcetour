<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar;

class CalendarController extends Controller
{
    public function index($id)
    {
        $data = Calendar::where('room_id', $id)->get();
        return view('backend.calendar.index',compact('data','id'));
    }
    public function create($id)
    {
        return view('backend.calendar.create',compact('id'));
    }
    public function edit($id)
    {
        $data = Calendar::findOrFail($id);
        return view('backend.calendar.edit',compact('data','city'));
    }
    public function show($id)
    {
        $data = Calendar::findOrFail($id);
        return view('backend.calendar.show',compact('data'));
    }
    public function store(Request $request, $id)
    {
        request()->validate([
            'from'=>'required',
            'to'=>'required',
        ]);
        Calendar::create([
            'from'=>$request->from,
            'to'=>$request->to,
            'room_id'=> $id
        ]);
    return redirect()->action('CalendarController@index',$id)->with('success','Успешно добавлено');
    }
}
