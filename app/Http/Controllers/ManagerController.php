<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\User;
use App\Hotel;
use App\City;

class ManagerController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('backend.users.index', compact('data'));
    }
    public function create()
    {
        $data = Role::all();
        $city = City::all();
        return view('backend.users.create', compact('data','city'));
    }
    public function edit($id)
    {
        $data = User::findOrFail($id);
        $role = Role::all();
        return view('backend.users.edit',compact('data','role'));
    }
    public function show($id)
    {
        $data = User::findOrFail($id);
        return view('backend.users.show',compact('data'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'phone' => 'required|unique:users',
        ]);
        $role = Role::where('type', request('role'))->first();
        $data = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'phone' => request('phone'),
            'password' => bcrypt(request('password'))
        ]);
        $data->roles()->attach($role);
    return redirect()->action('ManagerController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        $role = Role::where('type', request('role'))->first();
        $user = User::find($id);
        if($request->password != null){
            $user->update(['password' => bcrypt(request('password'))]);
        }
        $user->update($request->except('password'));
        $user->roles()->sync($role);

        return redirect()->action('ManagerController@index')->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = User::findOrFail($id);
        $hotel->delete();
        return redirect()->action('ManagerController@index')->with('success','Успешно удален');
    }
}
