<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceInner;
use App\Service;

class ServiceInnerController extends Controller
{
    public function index($id)
    {
        $data = ServiceInner::where('service_id',$id)->get();
        $info = Service::find($id);
        return view('backend.service-inner.index',compact('data','info'));
    }
    public function create($id)
    {
        return view('backend.service-inner.create', compact('id'));
    }
    public function edit($id)
    {
        $data = ServiceInner::findOrFail($id);
        return view('backend.service-inner.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = ServiceInner::findOrFail($id);
        return view('backend.service-inner.show',compact('data'));
    }
    public function store(Request $request,$id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        ServiceInner::create([
            'name_ru'=>$request->name_ru,
            'name_en'=>$request->name_en,
            'service_id'=> $id,
        ]);
    return redirect()->action('ServiceInnerController@index',$id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        ServiceInner::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('ServiceInnerController@index',$id)->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = ServiceInner::findOrFail($id);
        $hotel->delete();
        return redirect()->action('ServiceInnerController@index',$id)->with('success','Успешно удален');
    }
}
