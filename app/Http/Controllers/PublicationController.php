<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publication;
use App\City;
use App\Category;

class PublicationController extends Controller
{
    public function index()
    {
        $data = Publication::all();
        return view('backend.publication.index', compact('data'));
    }
    public function create()
    {
        $data = City::all();
        $category = Category::all();
        return view('backend.publication.create', compact('data','category'));
    }
    public function edit($id)
    {
        $data = Publication::findOrFail($id);
        $city = City::all();
        $category = Category::all();
        return view('backend.publication.edit',compact('data','city','category'));
    }
    public function show($id)
    {
        $data = Publication::findOrFail($id);
        return view('backend.publication.show',compact('data'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
            'text1_ru' => 'required',
            'text1_en' => 'required',
        ]);
        Publication::create([
            'city_id'=>$request->city_id,
            'category_id'=>$request->category_id,
            'name_ru'=>$request->name_ru,
            'name_en'=>$request->name_en,
            'text1_ru'=>$request->text1_ru,
            'text2_ru'=>$request->text2_ru ? $request->text2_ru : null,
            'text3_ru'=>$request->text3_ru ? $request->text3_ru : null,
            'text1_en'=>$request->text1_en,
            'text2_en'=>$request->text2_en ? $request->text2_en : null,
            'text3_en'=>$request->text3_en ? $request->text3_en : null,
        ]);
    return redirect()->action('PublicationController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        Publication::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('PublicationController@index')->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = Publication::findOrFail($id);
        $hotel->delete();
        return redirect()->action('PublicationController@index')->with('success','Успешно удален');
    }
}
