<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use Intervention\Image\ImageManagerStatic as Image;

class TypeController extends Controller
{
    public function index()
    {
        $data = Type::all();
        return view('backend.type.index',compact('data'));
    }
    public function create()
    {
        return view('backend.type.create');
    }
    public function edit($id)
    {
        $data = Type::findOrFail($id);
        return view('backend.type.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = Type::findOrFail($id);
        return view('backend.type.show',compact('data'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'text_ru' => 'required',
            'text_en' => 'required',
        ]);
        $type = Type::create(
            $request->except('image')
        );
        $image = $request->image;
        $this->storeImages($image,$type->id);
    return redirect()->back()->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        Type::findOrFail($id)->update(
            $request->except('image','_method','method')
        );
        if($request->image){
            $pathToDestroy = public_path('uploads/type/' . $id . '.jpg');
            if(!\File::isDirectory($pathToDestroy)) {
                \File::delete($pathToDestroy);
            }
            $this->createFolder('type');
            $this->storeImages($request->image,$id);
        }
        return redirect()->action('TypeController@index')->with('success','Изменения успешно внесены');
    }
    public function switch(Request $request, $id)
    {
        $hotel = Type::findOrFail($id);
        $hotel->update([
            'status'=> !$hotel->status
        ]);
        return redirect()->action('TypeController@index')->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = Type::findOrFail($id);
        $hotel->delete();
        return redirect()->action('TypeController@index')->with('success','Успешно удален');
    }
    public function createFolder($directory)
    {
        $directory_path = public_path('uploads/'.$directory);
        if(!\File::isDirectory($directory_path)){
            \File::makeDirectory($directory_path,0777,true,true);
        }
        return 1;
    }
    public function storeImages($image,$serviceId)
    {
        $filename = $serviceId .'.jpg';
        $path = public_path('uploads/type/'. $filename);
        Image::make($image->getRealPath())->encode('jpg', 90)
        ->fit(264, 264)
        ->save($path);
        return 1;
    }
}
