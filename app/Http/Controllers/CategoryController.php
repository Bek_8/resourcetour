<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::all();
        return view('backend.categories.index',compact('data'));
    }
    public function create()
    {
        $data = City::all();
        return view('backend.categories.create', compact('data'));
    }
    public function edit($id)
    {
        $data = Category::findOrFail($id);
        return view('backend.categories.edit',compact('data','city'));
    }
    public function show($id)
    {
        $data = Category::findOrFail($id);
        return view('backend.categories.show',compact('data'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        Category::create([
            'name_ru'=>$request->name_ru,
            'name_en'=>$request->name_en,
        ]);
    return redirect()->action('CategoryController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        Category::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('CategoryController@index')->with('success','Изменения успешно внесены');
    }
    public function switch(Request $request, $id)
    {
        $hotel = Category::findOrFail($id);
        $hotel->update([
            'status'=> !$hotel->status
        ]);
        return redirect()->action('CategoryController@index')->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = Category::findOrFail($id);
        $hotel->delete();
        return redirect()->action('CategoryController@index')->with('success','Успешно удален');
    }
}
