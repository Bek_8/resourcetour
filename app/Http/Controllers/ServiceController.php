<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Intervention\Image\ImageManagerStatic as Image;

class ServiceController extends Controller
{
    public function index()
    {
        $data = Service::all();
        return view('backend.service.index',compact('data'));
    }
    public function create()
    {
        return view('backend.service.create');
    }
    public function edit($id)
    {
        $data = Service::findOrFail($id);
        return view('backend.service.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = Service::findOrFail($id);
        return view('backend.service.show',compact('data'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        $service = Service::create([
            'name_ru'=>$request->name_ru,
            'name_en'=>$request->name_en,
        ]);
        $image = $request->image;
        $this->storeImages($image,$service->id);
        return redirect()->action('ServiceController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        Service::findOrFail($id)->update(
            $request->except('image')
        );
        if($request->image){
            $pathToDestroy = public_path('uploads/service/' . $id . '.jpg');
            if(!\File::isDirectory($pathToDestroy)) {
                \File::delete($pathToDestroy);
            }
            $this->storeImages($request->image,$id);
        }
        return redirect()->action('ServiceController@index')->with('success','Изменения успешно внесены');
    }
    public function storeImages($image,$serviceId)
    {
        $filename = $serviceId .'.jpg';
        $path = public_path('uploads/service/'. $filename);
        Image::make($image->getRealPath())->encode('jpg', 90)
        ->fit(264, 264)
        ->save($path);
        return 1;
    }
    public function delete($id)
    {
        $hotel = Service::findOrFail($id);
        $hotel->delete();
        return redirect()->action('ServiceController@index')->with('success','Успешно удален');
    }
}
