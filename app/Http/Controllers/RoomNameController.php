<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomName;
use App\Type;

class RoomNameController extends Controller
{
    public function index($id)
    {
        $data = RoomName::where('type_id',$id)->get();
        $info = Type::find($id);
        return view('backend.room-name.index',compact('data','info','id'));
    }
    public function create($id)
    {
        return view('backend.room-name.create', compact('id'));
    }
    public function edit($id)
    {
        $data = RoomName::findOrFail($id);
        return view('backend.room-name.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = RoomName::findOrFail($id);
        return view('backend.room-name.show',compact('data'));
    }
    public function store(Request $request,$id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        RoomName::create([
            'name_ru'=>$request->name_ru,
            'name_en'=>$request->name_en,
            'type_id'=> $id,
        ]);
    return redirect()->action('RoomNameController@index',$id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        RoomName::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('RoomNameController@index',$id)->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = RoomName::findOrFail($id);
        $hotel->delete();
        return redirect()->action('RoomNameController@index',$id)->with('success','Успешно удален');
    }
}
