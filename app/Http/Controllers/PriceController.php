<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Hotel;

class PriceController extends Controller
{
    public function index($id)
    {
        $data = Room::where('hotel_id',$id)->get();
        return view('backend.price.index',compact('data','id'));
    }
    public function create($id)
    {
        $hotel = Hotel::findOrFail($id);
        return view('backend.price.create',compact('id','hotel'));
    }
    public function edit($id)
    {
        $data = Room::findOrFail($id);
        return view('backend.price.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = Room::findOrFail($id);
        return view('backend.price.show',compact('data'));
    }
    public function store(Request $request, $id)
    {
        request()->validate([
            "name" => 'required',
            "phone" => 'required',
            "email" => 'required',
        ]);
        $create_data = $request->all();
        $create_data['hotel_id'] = $id;
        $create_data['enabled'] = true;
        Room::create($create_data);

    return redirect()->action('PriceController@index',$id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        $hotel_id = $request->hotel_id;
        $rooms = Room::where('hotel_id',$hotel_id)->get();
        $price = $request->price;
        foreach ($rooms as $key => $room){
            $capacity = $room->capacity;
            $new_price[$key] = [];
            for($i = 0 + $key;  $i < $capacity + $key; $i++){
                array_push($new_price[$key],$price[$i]);
            }
            $room->update(['price' => json_encode($new_price[$key])]);
        }
        return redirect()->action('PriceController@index',$id)->with('success','Изменения успешно внесены');
    }
    public function switch(Request $request, $id)
    {
        $hotel = Room::findOrFail($id);
        $hotel->update([
            'enabled'=> !$hotel->enabled
        ]);
        return redirect()->action('PriceController@index',$id)->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = Room::findOrFail($id);
        $hotel->delete();
        return redirect()->action('PriceController@index',$id)->with('success','Успешно удален');
    }
    public function createFolder($directory,$roomId)
    {
        $path = public_path('uploads/'.$directory.'/'.$roomId);
        $directory_path = public_path('uploads/'.$directory);
        if(!\File::isDirectory($directory_path)){
            \File::makeDirectory($directory_path,666,true);
        }
        if(!\File::isDirectory($path)){
            \File::makeDirectory($path,666,true);
        }
        return 1;
    }
    public function storeImages($images,$roomId)
    {
        foreach($images as $key => $image){
            $filename = $key+strtotime("now") .'.jpg';
            $path = public_path('uploads/room/'.$roomId.'/'. $filename);
            Image::make($image->getRealPath())->encode('jpg', 90)
            ->fit(945, 630)
            ->save($path);
        }
        return 1;
    }
    public function removeImage($id)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/room/'.$id.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay',200);
    }
}
