<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function room()
    {
        return $this->belongsTo('App\Room');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }
    public function getNights()
    {
        $from = Carbon::parse($this->from);
        $to = Carbon::parse($this->to);
        return $from->diffInDays($to);
    }
}
